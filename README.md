# TUNE-IN
##### Tran Le & Brandon Anderson
---
###### Tune-In creates a friendly environment for DJs and audience to communicate with each other. As we researched, we figured out that not many people use cash nowadays to tip their DJs and there are so many articles for how to tip their DJs so we thought that if the audience has to think about this and cannot enjoy their time. Therefore, Tune-In is made to assist and make this process easier and less stressful for the users. Besides that, Tune-In also allows audience to search for songs and request them. The audience could scan DJ's QR code, visit their profile and follow them. DJs can see their followers and data visualization for his tips weekly, monthly or yearly.

---
1. **Available Features**
- Log in / sign up for new account.
- Using current location to scan for nearby DJs.
- Search for song to request.
- Send tip to DJ.
- Scan app QR code / DJ's unique QR code.
- Follow DJs.
- View DJ profile.
- View tip data visualization.

2. **Installation considerations and requirements**: no information.

3. **Hardware considerations and requirements**: on iOS devices with opertation system of 10.0 and above.

4. **Login requirements for testing**
- Log in as audience:
- Email: user_testing@gmail.com
- Password: Password1

- Log in as DJ:
- Email: dj_testing@gmail.com
- Password: Password1

- Sign up: you can sign up with any email and password that you want.

5. **List of known bugs**: no information.

6. **Special requirements and considerations for testing**: no information.