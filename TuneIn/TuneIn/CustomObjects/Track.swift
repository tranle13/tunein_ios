//
//  Track.swift
//  TuneIn
//
//  Created by Sunny Le on 12/16/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import UIKit

class Track {
    
    // Variables
    private var name: String
    private var artist: String
    private var url : String
    private var cover: UIImage
   
    
    // Initializer
    init(name: String, artist: String, cover: String) {
        self.name = name
        self.artist = artist
        self.url = cover
        var cover_image = UIImage()
        // Code to get UIImage from string URL
        if let url = URL(string: cover) {
            do {
                let data = try Data(contentsOf: url)
                if let image = UIImage(data: data) {
                    cover_image = image
                } else {
                    cover_image = #imageLiteral(resourceName: "NoCover")
                }
            } catch {
                print(error.localizedDescription)
            }
        } else {
            cover_image = #imageLiteral(resourceName: "NoCover")
        }
        
        self.cover = cover_image
    }
    
    // Getters
    var getName: String {
        get {
            return name
        }
    }
    
    var getURL : String{
        get{
            return url
        }
    }
    
    var getArtist: String {
        get {
            return artist
        }
    }
    
    var getCover: UIImage {
        get {
            return cover
        }
    }
}

