//
//  CustomLiveDJTableCell.swift
//  TuneIn
//
//  Created by Sunny Le on 1/11/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit

class CustomLiveDJTableCell: UITableViewCell {

    @IBOutlet weak var imageview_DJImage: UIImageView!
    @IBOutlet weak var label_DJName: UILabel!
    @IBOutlet weak var imageview_CurrentDJSignage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
