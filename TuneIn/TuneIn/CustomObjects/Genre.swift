//
//  Genre.swift
//  TuneIn
//
//  Created by Sunny Le on 12/13/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import Foundation

class Genre {
    
    // Variables
    private var genreName: String
    private var genreState: Bool
    
    // Initializer
    init(name: String, state: Bool) {
        self.genreName = name
        self.genreState = state
    }
    
    // Getters and setters
    var getGenreName: String {
        get {
            return genreName
        }
    }
    
    var getGenreState: Bool {
        set {
            genreState = newValue
        }
        get {
            return genreState
        }
    }
}
