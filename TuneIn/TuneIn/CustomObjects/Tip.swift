//
//  Tip.swift
//  TuneIn
//
//  Created by Sunny Le on 1/18/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import Foundation

class Tip {
    
    var amount: Double
    var day: Int
    var month: Int
    var year: Int
    var tipperUid: String
    
    init(_amount: Double, _day: Int, _month: Int, _year: Int, _tipper: String) {
        amount = _amount
        day = _day
        month = _month
        year = _year
        tipperUid = _tipper
    }
}
