//
//  CustomTextField.swift
//  TuneIn
//
//  Created by Sunny Le on 12/4/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        if self.placeholder == "Music genre" {
            return UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, 10, 0, 20))
        }
        return UIEdgeInsetsInsetRect(bounds,
                                     UIEdgeInsetsMake(0, 10, 0, 10))
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds,
                                     UIEdgeInsetsMake(0, 10, 0, 10))
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds,
                                     UIEdgeInsetsMake(0, 10, 0, 10))
    }
    
    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = self.frame.height / 2 - 10
        self.layer.masksToBounds = true
    }
}
