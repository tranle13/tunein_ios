//
//  DJ Profile.swift
//  TuneIn
//
//  Created by Sunny Le on 1/11/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import Firebase

class DJProfile {
    
    // Variables
    private var name: String
    private var uid: String
    private var email: String
    
    // Initializer
    init(name: String, uid: String, email: String) {
        self.name = name
        self.uid = uid
        self.email = email
    }
    
    // Getters
    var getName: String {
        get {
            return name
        }
    }
    
    var getUid: String {
        get {
            return uid
        }
    }
    
    var getEmail: String {
        get {
            return email
        }
    }
    
}
