//
//  CustomTrackTableCellTableViewCell.swift
//  TuneIn
//
//  Created by Sunny Le on 12/16/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import UIKit

class CustomTrackTableCell: UITableViewCell {

    @IBOutlet weak var imageview_TrackImage: UIImageView!
    @IBOutlet weak var label_TrackName: UILabel!
    @IBOutlet weak var label_TrackArtist: UILabel!
    @IBOutlet weak var button_SendRequest: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
