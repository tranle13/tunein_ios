//
//  CustomGenreTableCell.swift
//  TuneIn
//
//  Created by Sunny Le on 12/13/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import UIKit

class CustomGenreTableCell: UITableViewCell {

    // MARK: Outlets
    @IBOutlet weak var button_CheckMark: UIButton!
    @IBOutlet weak var label_Genre: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        button_CheckMark.layer.cornerRadius = button_CheckMark.frame.height / 2
        button_CheckMark.contentEdgeInsets = UIEdgeInsetsMake(2, 2, 2, 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
