//
//  RequestedSongsTableViewCell.swift
//  TuneIn
//
//  Created by Brandon Anderson on 12/16/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import UIKit

class RequestedSongsTableViewCell: UITableViewCell {

    // Outlets
    @IBOutlet weak var button_accept: UIButton!
    @IBOutlet weak var button_reject: UIButton!
    @IBOutlet weak var label_Title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
