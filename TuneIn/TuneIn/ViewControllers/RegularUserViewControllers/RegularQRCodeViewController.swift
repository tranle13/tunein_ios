//
//  QRCodeViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 1/9/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import AVFoundation

class RegularQRCodeViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    // MARK: Outlets
    @IBOutlet weak var view_AppCodeView: UIView!
    @IBOutlet weak var imageview_QRCode: UIImageView!
    @IBOutlet weak var segment_QRSegment: UISegmentedControl!
    @IBOutlet weak var view_ScannerView: UIView!
    @IBOutlet weak var label_NoQrDetected: UILabel!
    @IBOutlet weak var imageview_ScannerFrame: UIImageView!
    
    // MARK: Variables
    let session = AVCaptureSession()
    var scannedDJ = ""
    var video = AVCaptureVideoPreviewLayer()
    
    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        removeBorderAddShadowToNavigationBar()
        
        checkSegment()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Function to change QR code based on section of segment
    @IBAction func segmentTapped(_ sender: UISegmentedControl) {
        checkSegment()
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count > 0 {
            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
                if object.type == AVMetadataObject.ObjectType.qr {
                    print("is this the qr?"+object.stringValue!)
                    scannedDJ = object.stringValue!
                     print("is this the qr?"+object.stringValue!)
                    if(scannedDJ.contains("realdj+")){
                        label_NoQrDetected.isHidden = true
                        session.stopRunning()
//                        scannedDJ.removeFirst()
//                        scannedDJ.removeFirst()
//                        scannedDJ.removeFirst()
//                        scannedDJ.removeFirst()
                         performSegue(withIdentifier: "GoToDj", sender: self)
                    } else {
                        label_NoQrDetected.isHidden = false
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? RegularViewDJViewController{
            let values = self.scannedDJ.components(separatedBy: "+")
            destination.selectedDJ = values[1]
        }
    }
    
    func checkSegment() {
        if segment_QRSegment.selectedSegmentIndex == 0 {
            view_AppCodeView.isHidden = false
            view_ScannerView.isHidden = true
        } else {
            view_AppCodeView.isHidden = true
            view_ScannerView.isHidden = false
            // Create session
            let session = AVCaptureSession()
            
            // Define capture device
            let captureDevice = AVCaptureDevice.default(for: .video)
            
            do {
                let input = try AVCaptureDeviceInput(device: captureDevice!)
                session.addInput(input)
            } catch {
                print(error.localizedDescription)
            }
            
            let output = AVCaptureMetadataOutput()
            session.addOutput(output)
            
            output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
            video = AVCaptureVideoPreviewLayer(session: session)
            video.frame = self.view.layer.bounds
            view_ScannerView.clipsToBounds = true
            view_ScannerView.layer.addSublayer(video)
            
            self.view_ScannerView.bringSubview(toFront: imageview_ScannerFrame)
            
            session.startRunning()
        }
    }
}
