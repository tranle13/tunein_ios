//
//  RegularLiveDJViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 1/11/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import Firebase

class RegularLiveDJViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var imageview_SelectedDJImage: UIImageView!
    @IBOutlet weak var label_SelectedDJName: UILabel!
    @IBOutlet weak var button_ChangeChosenDJ: UIButton!
    @IBOutlet weak var button_FollowDJ: UIButton!
    
    // MARK: Variables
    var selectedDJ: DJProfile!
    var ref: DatabaseReference!
    var following = false
    var isCurrentDJ = false
    var name = ""

    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageview_SelectedDJImage.layer.cornerRadius = imageview_SelectedDJImage.frame.height / 2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Do any additional setup after loading the view.
        button_ChangeChosenDJ.layer.cornerRadius = button_ChangeChosenDJ.frame.height / 2
        
        ref = Database.database().reference()
        
        let userID = Auth.auth().currentUser?.uid
        ref.child("dj_users").child(selectedDJ.getUid).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let followers = value!["followers"] as? [String : Any]
            
            if (followers?.index(forKey: userID!) != nil){
                self.following = true
                self.button_FollowDJ.setTitle("UNFOLLOW", for: .normal)
            }else{
                self.following = false
                self.button_FollowDJ.setTitle("FOLLOW", for: .normal)
            }
            
            if let urlString = value?["image"] as? String {
                let url = URL(string: urlString)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!)
                    DispatchQueue.main.async {
                        if(data != nil){
                            self.imageview_SelectedDJImage.image = UIImage(data: data!)
                        }
                       
                    }
                }
            }
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
        
        ref.child("regular_users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            self.name = (value!["fullname"] as? String)!
            if let currentdj = value!["currentLiveDJ"] as? String{
                if(currentdj != self.selectedDJ.getUid){
                    self.isCurrentDJ = false
                    self.button_ChangeChosenDJ.setTitle("SET AS YOUR DJ", for: .normal)
                }else{
                    self.isCurrentDJ = true
                    self.button_ChangeChosenDJ.setTitle("REMOVE AS YOUR DJ", for: .normal)
                }
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
        label_SelectedDJName.text = selectedDJ.getName
        
        roundButtons(buttons: [button_ChangeChosenDJ, button_FollowDJ])
        button_ChangeChosenDJ.layer.borderWidth = 1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Function to allow regular users to follow DJ
    @IBAction func FollowDJ(_ sender: Any) {
        if(!following){
       let token : [String: AnyObject] = [Messaging.messaging().fcmToken!: Messaging.messaging().fcmToken as AnyObject]
        ref.child("regular_users").child((Auth.auth().currentUser?.uid)!).child("followed_djs").child(selectedDJ.getUid).setValue(selectedDJ.getName)
        ref.child("dj_users").child(selectedDJ.getUid).child("followers").child((Auth.auth().currentUser?.uid)!).setValue("following")
        let alert = UIAlertController(title: "Followed", message: "You successfully followed this DJ", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
            button_FollowDJ.setTitle("Unfollow", for: .normal)
            
            
        ref.child("dj_users").child(selectedDJ.getUid).child("notifications").child("new_followers").child((Auth.auth().currentUser?.uid)!).setValue("\(self.name) is now following you!")
        }
        else{
            ref.child("regular_users").child((Auth.auth().currentUser?.uid)!).child("followed_djs").child(selectedDJ.getUid).removeValue()
            ref.child("dj_users").child(selectedDJ.getUid).child("followers").child((Auth.auth().currentUser?.uid)!).removeValue()
            ref.child("dj_users").child(selectedDJ.getUid).child("notifications").child("new_followers").child((Auth.auth().currentUser?.uid)!).removeValue()
             button_FollowDJ.setTitle("Follow", for: .normal)
            
            
        }
        following = !following
    }
    
    // Function to go back and change selected live DJ
    @IBAction func changeLiveDJTapped(_ sender: UIButton) {
        if (isCurrentDJ) {
            ref.child("regular_users").child(Auth.auth().currentUser?.uid ?? "").child("currentLiveDJ").removeValue()
            self.navigationController?.popViewController(animated: true)
        }else{
            ref.child("regular_users").child(Auth.auth().currentUser?.uid ?? "").child("currentLiveDJ").setValue(selectedDJ.getUid)
        self.navigationController?.popViewController(animated: true)
        }
    }
    
    
}
