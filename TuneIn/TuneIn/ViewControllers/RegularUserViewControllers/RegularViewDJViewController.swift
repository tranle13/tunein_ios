//
//  RegularViewDJViewController.swift
//  TuneIn
//
//  Created by Brandon Anderson on 1/20/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import Firebase

class RegularViewDJViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var uiimageProfile: UIImageView!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnTip: UIButton!
    
    // MARK: Variables
    var ref: DatabaseReference!
    var selectedDJ : String!
    var following : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference()
        
        if selectedDJ != nil {
            getDJProfile()
        }
        // Do any additional setup after loading the view.
        roundButtons(buttons: [btnFollow, btnTip])
        uiimageProfile.layer.cornerRadius = uiimageProfile.frame.height / 2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getDJProfile()
    }
    
    @IBAction func followunfollowDJ(_ sender: Any) {
        if following{
            following = false
            ref.child("dj_users").child(selectedDJ!).child("followers").child((Auth.auth().currentUser?.uid)!).removeValue()
            ref.child("regular_users").child((Auth.auth().currentUser?.uid)!).child("followed_djs").child(selectedDJ!).removeValue()
            btnFollow.setTitle("FOLLOW", for: .normal)
            return
        } else {
            following = true
            ref.child("dj_users").child(selectedDJ).child("followers").child((Auth.auth().currentUser?.uid)!).setValue("following")
            ref.child("regular_users").child((Auth.auth().currentUser?.uid)!).child("followed_djs").child(selectedDJ!).setValue(lblName.text)
            btnFollow.setTitle("UNFOLLOW", for: .normal)

            return
        }
    }
    
    @IBAction func tipDJ(_ sender: Any) { }
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func getDJProfile() {
        let userID = Auth.auth().currentUser?.uid
        
        ref.child("dj_users").child(selectedDJ!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let name = value?["fullname"] as? String
            
            if let imageUrl = value?["image"] as? String {
                let url = URL(string: imageUrl)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!)
                    DispatchQueue.main.async {
                        if(data != nil){
                             self.uiimageProfile.image = UIImage(data: data!)
                        }
                       
                    }
                }
            }
            
            self.lblName.text = name
            
            let followers = value?["followers"] as? [String:Any]
            if ( followers == nil || (followers?.isEmpty)!){
                return
            }
            if followers![userID!] != nil{
                self.following = true
                 self.btnFollow.setTitle("UNFOLLOW", for: .normal)
            } else {
                self.following = false
                 self.btnFollow.setTitle("FOLLOW", for: .normal)
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
