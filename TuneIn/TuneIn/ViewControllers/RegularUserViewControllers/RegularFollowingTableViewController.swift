//
//  RegularFollowingTableViewController.swift
//  TuneIn
//
//  Created by Brandon Anderson on 1/20/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import Firebase

class RegularFollowingTableViewController:  UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: Variables
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var view_EmptyState: UIView!
    
    // MARK: Outlets
    var ref: DatabaseReference!
    var followedDjs: [(uid: String, name: String)] = []
    
    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ref = Database.database().reference()
        followedDjs.removeAll()
        tableview.reloadData()
        getDJs()
    }

    // Function to get all the DJs that the user follows
    func getDJs(){
        let userID = Auth.auth().currentUser?.uid
        ref.child("regular_users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            guard let trueValue = snapshot.value as? [String: Any] else {
                  self.tableview.reloadData()
                return
            }
            
            if let followedDJs = trueValue["followed_djs"] as? [String: String] {
                followedDJs.forEach { (key, value) in
                    self.followedDjs.append((key, value))
                }
                self.tableview.reloadData()
            }
            
        }) { (error) in
              self.tableview.reloadData()
            print(error.localizedDescription)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? RegularViewDJViewController {
            destination.selectedDJ = followedDjs[(tableview.indexPathForSelectedRow?.row)!].uid
        }
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if followedDjs.count != 0 {
            view_EmptyState.isHidden = true
        }else{
            view_EmptyState.isHidden = false
        }
        return followedDjs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = followedDjs[indexPath.row].name
        return cell
    }
}
