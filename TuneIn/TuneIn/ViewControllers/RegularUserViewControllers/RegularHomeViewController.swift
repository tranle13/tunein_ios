//
//  HomeViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 12/4/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation

class RegularHomeViewController: UIViewController, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: Outlets
    @IBOutlet weak var tableview_LiveDJList: UITableView!
    @IBOutlet weak var view_NoLiveDJs: UIView!
    
    // MARK: Variables
    var ref: DatabaseReference!
    var locationManager: CLLocationManager!
    var liveDJs: [DJProfile] = []
    var djImages: [String] = []
    var selectedDJ: DJProfile!
    var currentDJ: String!
    let process: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    var timer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Call function to remove the border for navigation bar and add shadow
        removeBorderAddShadowToNavigationBar()
        
        ref = Database.database().reference().child("dj_users")
        
        // Timer to repeat checking location and scan for nearby live DJs for regular user
        timer = Timer.scheduledTimer(timeInterval: 900, target: self, selector: #selector(determineMyCurrentLocation), userInfo: nil, repeats: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getDJ(){
        let userID = Auth.auth().currentUser?.uid
        ref = Database.database().reference()
        ref.child("regular_users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
           
            self.currentDJ = value!["currentLiveDJ"] as? String ?? ""
            
             self.tableview_LiveDJList.reloadData()
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        process.center = self.view.center
        self.view.addSubview(process)
        process.bringSubview(toFront: self.view)
        process.hidesWhenStopped = true
       
        determineMyCurrentLocation()
        getDJ()
       
    }
    
    // Function to create instance for CLLocationManager, set delegate to this class to find current location of user
    @objc func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    // Function to get the current location and information of live DJ near current regular user
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation: CLLocation = locations[0] as CLLocation
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        ref = Database.database().reference().child("dj_users")
        ref.observe(.value, with: { (snapshot) in
            self.liveDJs.removeAll()
            
            guard let shotValue = snapshot.value as? [String: Any] else {
                return
            }
            
            shotValue.forEach({ (key, value) in
                guard let realValue = value as? [String: Any] else {
                    return
                }
                
                if let isLive = realValue["isLive"] as? Bool, isLive == true {
                    if let latitude = CLLocationDegrees(exactly: (realValue["latitude"] as? NSNumber)!), let longitude = CLLocationDegrees(exactly: (realValue["longitude"] as? NSNumber)!) {
                        let djLocation = CLLocation(latitude: latitude, longitude: longitude)
                        let distance = userLocation.distance(from: djLocation)
                        
                        if distance >= 0, distance <= 3500 {
                            self.djImages.append(realValue["image"] as? String ?? "")
                            self.liveDJs.append(DJProfile(name: realValue["fullname"] as? String ?? "No Name", uid: key, email: realValue["email"] as? String ?? "No Email"))
                        }
                    }
                }
            })
            
                self.process.stopAnimating()
                self.tableview_LiveDJList.reloadData()
                
                if self.liveDJs.count > 0 {
                    self.view_NoLiveDJs.isHidden = true
                } else {
                    self.view_NoLiveDJs.isHidden = false
                }
        }) { (error) in
            print(error.localizedDescription)
        }
    
        // Call stopUpdatingLocation() to stop listening for location updates otherwise this function will be called every time when user location changes
        manager.stopUpdatingLocation()
    }
    
    // Function to print out error if something happens to the getting location name process
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    // MARK: Conforms UITableView's protocol stubs
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if liveDJs.count > 0 {
            view_NoLiveDJs.isHidden = true
        } else {
            view_NoLiveDJs.isHidden = false
        }
        return liveDJs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "liveDJCell", for: indexPath) as? CustomLiveDJTableCell else {
            return tableView.dequeueReusableCell(withIdentifier: "liveDJCell", for: indexPath)
        }
        
        let url = URL(string: djImages[indexPath.row])
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!)
            DispatchQueue.main.async {
                if data != nil {
                    cell.imageview_DJImage.image = UIImage(data: data!)
                }
            }
        }
        
        if(liveDJs[indexPath.row].getUid == currentDJ){
            cell.imageview_CurrentDJSignage.isHidden = false
        }else{
            cell.imageview_CurrentDJSignage.isHidden = true
        }
        
        cell.label_DJName.text = liveDJs[indexPath.row].getName
        cell.imageview_DJImage.layer.cornerRadius =  cell.imageview_DJImage.frame.height / 2 + 1
        
        return cell
    }
    
    // Function to navigate to new window after user selects their live DJ
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedDJ = liveDJs[indexPath.row]
        performSegue(withIdentifier: "ToSetLiveDJ", sender: self)
    }
    
    // Function to pass info of selected DJ to the next RegularLiveDJViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? RegularLiveDJViewController {
            destination.selectedDJ = self.selectedDJ
        }
    }
}
