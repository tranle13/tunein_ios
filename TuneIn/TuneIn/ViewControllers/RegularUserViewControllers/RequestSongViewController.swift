//
//  RequestSongViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 12/13/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import FirebaseDatabase
import FirebaseAuth
import UIKit

class RequestSongViewController: UIViewController, UISearchResultsUpdating, UISearchBarDelegate {
    
    // MARK: Outlets
    @IBOutlet weak var trackart: UIImageView!
    @IBOutlet weak var trackartist: UILabel!
    @IBOutlet weak var tracktitle: UILabel!
    @IBOutlet weak var HasSongRequestLbl: UILabel!
    @IBOutlet weak var view_EmptyState: UIView!
    
    // MARK: Variables
    var hasDJ = false
    var selectedSong = ""
    var currentDJ = ""
    let cellIdentifier = "reuseIdentifier"
    
    // MARK: Variables
    var songs = [String]()
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        removeBorderAddShadowToNavigationBar()
        
        ref = Database.database().reference()
         let userID = Auth.auth().currentUser?.uid
        
        ref.child("regular_users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let djID = value?["currendLiveDJ"] as? String
           
            if djID != nil{
                
                //We have a DJ they can select a song
                self.currentDJ = djID!
                self.hasDJ = true
                
            } else {
                
                // We dont have a DJ they cant select a song
                self.hasDJ = false
            }
        }) { (error) in
            print(error.localizedDescription)
        }

    }
    
    // Update the requested song list when view appears
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.15
        
        let userID = Auth.auth().currentUser?.uid
        ref.child("regular_users").child(userID!).child("requested_song").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let artist = value?["artist"] as? String ?? ""
            let title = value?["title"] as? String ?? ""
            let url =  value?["url"] as? String ?? ""
            
            let track = Track.init(name: title, artist: artist, cover: url)
            
            if track.getName != "", track.getArtist != "" {
                self.trackart.image = track.getCover
                self.view_EmptyState.isHidden = true
            } else {
                self.trackart.image = UIImage()
                self.view_EmptyState.isHidden = false
            }
            
            
            if let url = URL(string: track.getURL) {
                do {
                    let data = try Data(contentsOf: url)
                    if let image = UIImage(data: data) {
                        self.trackart.image = image
                    } else {
                        self.trackart.image = #imageLiteral(resourceName: "NoCover")
                    }
                } catch {
                    print(error.localizedDescription)
                }
            }else{
                print(track.getURL)
            }
            
            self.tracktitle.text = track.getName
            self.trackartist.text = track.getArtist
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func updateSearchResults(for searchController: UISearchController) {
        // Update result based on search term
    }
    
//    // Function to get the requested song from current live DJ
//    @IBAction func RequestSong(_ sender: Any) {
//        if(selectedSong != ""){
//            self.ref.child("dj_users").child(currentDJ).child("requested_songs").child(Auth.auth().currentUser?.uid ?? "test").child(trackartist.text!).setValue(selectedSong)
//            
//            let alert = UIAlertController(title: "Requested Song", message: "Successfully requested your song! Requesting another song will replace this.", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
//                NSLog("The \"OK\" alert occured.")
//            }))
//            self.present(alert, animated: true, completion: nil)
//        }
//    }
    
   
}
