//
//  RegularNotificationsTableViewController.swift
//  TuneIn
//
//  Created by Brandon Anderson on 1/25/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class RegularNotificationsTableViewController:UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    // MARK: Variables
    var isDJ = false
    var deleting = false
    var ref: DatabaseReference!
    var tips = [Tip]()
    var djswentlive = [String]()
    var tipsnotifications = [String]()
    
    // MARK: Conform UITableView's protocol stubs
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch NotificationSegmentControler.selectedSegmentIndex {
        case 0:
            if isDJ {
                hideShowEmptyState(tips: tipsnotifications)
                return tipsnotifications.count
            }
            
            if tips.count > 0 {
                view_EmptyState.isHidden = true
            } else {
                view_EmptyState.isHidden = false
            }
            return tips.count
        default:
            hideShowEmptyState(tips: djswentlive)
            return djswentlive.count
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        switch NotificationSegmentControler.selectedSegmentIndex {
        case 0:
            if(isDJ){
                let msg = tipsnotifications[indexPath.row]
                 cell.textLabel?.text = msg
            }else{
            let tip = String(format: "Tip Amount: $%.02f", tips[indexPath.row].amount)
            let msg = "DJ: \(tips[indexPath.row].tipperUid) \(tip)"
            cell.textLabel?.text = msg
            }
        default:
           
            cell.textLabel?.text = djswentlive[indexPath.row]
        }
        
        return cell
    }
    
    // MARK: Outlets
    @IBOutlet weak var NotificationTableView: UITableView!
    @IBOutlet weak var NotificationSegmentControler: UISegmentedControl!
    @IBOutlet weak var button_TrashNotifications: UIBarButtonItem!
    @IBOutlet weak var view_EmptyState: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        if isDJ {
            
            self.djswentlive.removeAll()
            ref = Database.database().reference().child("dj_users").child((Auth.auth().currentUser?.uid)!)
                .child("notifications").child("new_followers")
            
            
            ref.observe(.value, with: { (snapshot) in
                self.djswentlive.removeAll()
                
                guard let shotValue = snapshot.value as? [String: Any] else {
                    return
                }
                
                shotValue.forEach({ (key, value) in
                    
                    
                    self.djswentlive.append(value as! String)
                    
                })
                 self.NotificationTableView.reloadData()
                
                
            }) { (error) in
                print(error.localizedDescription)
            }
            
            
            self.tipsnotifications.removeAll()
            ref = Database.database().reference().child("dj_users").child((Auth.auth().currentUser?.uid)!)
                .child("notifications").child("tips")
            
            ref.observe(.value, with: { (snapshot) in
               
                guard let shotValue = snapshot.value as? [String: Any] else {
                    return
                }
                
                shotValue.forEach({ (key, value) in
                    
                    let real  = value as? [String : String]
                    for val in (real?.values)!{
                        self.tipsnotifications.append(val)
                    }
                    self.NotificationTableView.reloadData()
                })
                
            }) { (error) in
                print(error.localizedDescription)
            }
            
           
            
        } else {
            ref = Database.database().reference().child("regular_users").child((Auth.auth().currentUser?.uid)!)
            .child("notifications").child("tips")
            
            
            ref.observe(.value, with: { (snapshot) in
                self.tips.removeAll()
                
                guard let shotValue = snapshot.value as? [String: Any] else {
                    return
                }
                
                shotValue.forEach({ (key, value) in
                    guard let realValue = value as? [String: Any] else {
                        return
                    }
                    
                    for v in realValue{
                        
                        let dateArray = v.key.components(separatedBy: "_")
                       
                        let day = dateArray[0]
                        let month = dateArray[1]
                        let year = dateArray[2]
                       
                        
                      //  print("A tip was given on this month ", month, " on this day " ,day , " on this hours ", hour)
                        self.ref = Database.database().reference()
                        self.ref.child("dj_users").child(key).observeSingleEvent(of: .value, with: { (snapshot) in
                            // Get user value
                            let value = snapshot.value as? NSDictionary
                            let name = value?["fullname"] as? String ?? ""
                            let tip = Tip.init(_amount: v.value as! Double, _day: Int(day)!, _month: Int(month)!, _year: Int(year)!, _tipper: name)
                            
                             self.tips.append(tip)
                               self.NotificationTableView.reloadData()
                        }) { (error) in
                            print(error.localizedDescription)
                        }
                    }
                    
                })
            
        
            }) { (error) in
                print(error.localizedDescription)
            }
        
            ref = Database.database().reference().child("regular_users").child((Auth.auth().currentUser?.uid)!)
                .child("notifications").child("live_djs")
            
            
            ref.observe(.value, with: { (snapshot) in
                self.djswentlive.removeAll()
                
                guard let shotValue = snapshot.value as? [String: Any] else {
                    return
                }
                
                shotValue.forEach({ (key, value) in
                   
                    
                    self.djswentlive.append(value as! String)
                    
                })
                
                
            }) { (error) in
                print(error.localizedDescription)
            }
        }
    }

    // Hide delete button if it's financial notifications and update the tableview
    @IBAction func SegmentChanged(_ sender: UISegmentedControl) {
        NotificationTableView.reloadData()
        if sender.selectedSegmentIndex == 0 {
            button_TrashNotifications.isEnabled = false
            button_TrashNotifications.tintColor = nil
        } else {
            button_TrashNotifications.isEnabled = true
            button_TrashNotifications.tintColor = UIColor(hexCode: "260909")
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // Function to edit the notification
    @IBAction func Editing(_ sender: UIBarButtonItem) {
        djswentlive.removeAll()
        if isDJ {
            ref = Database.database().reference()
            ref.child("dj_users").child((Auth.auth().currentUser?.uid)!).child("notifications").child("new_followers").removeValue()
        } else {
            ref = Database.database().reference()
            ref.child("regular_users").child((Auth.auth().currentUser?.uid)!).child("notifications").child("live_djs").removeValue()
        }
        NotificationTableView.reloadData()
    }
    
    // Function to show/hide empty state
    func hideShowEmptyState(tips: [String]) {
        if tips.count > 0 {
            view_EmptyState.isHidden = true
        } else {
            view_EmptyState.isHidden = false
        }
    }
}
