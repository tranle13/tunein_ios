//
//  RegularTipViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 1/9/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import PassKit
import FirebaseAuth
import Firebase

class RegularTipViewController: UIViewController, PKPaymentAuthorizationViewControllerDelegate, PKPaymentAuthorizationControllerDelegate {
    func paymentAuthorizationControllerDidFinish(_ controller: PKPaymentAuthorizationController) {
         controller.dismiss(completion: nil)
    }
    
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
         controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Outlets
    @IBOutlet weak var total: UILabel!
    @IBOutlet var button_Tip: PKPaymentButton!
    @IBOutlet weak var currentDJNameLbl: UILabel!
    @IBOutlet weak var dj_profileimg: UIImageView!
    @IBOutlet weak var hasCurrentDjView: UIView!
    @IBOutlet weak var noCurrentDjView: UIView!
    
    // MARK: Variables
    var paymentRequest = PKPaymentRequest()
    var ref: DatabaseReference!
    var currentDJ = ""
    var value = 0.0
    var userName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(UIApplication.shared.keyWindow!.rootViewController == self){
            print("we are the controller")
        }else{
            print("we are not...")
        }
        
        dj_profileimg.layer.cornerRadius =  dj_profileimg.frame.height / 2 + 1
 
        // Do any additional setup after loading the view.
        removeBorderAddShadowToNavigationBar()
        button_Tip.isEnabled = false
        roundButtons(buttons: [button_Tip])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("HELLO WORLD")
        ref = Database.database().reference()
        let userID = Auth.auth().currentUser?.uid
        
        
        
        ref.child("regular_users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let djID = value?["currentLiveDJ"] as? String
            self.userName = (value?["fullname"] as? String)!
            
            if djID != nil{
                //We have a DJ they can select a song
                self.currentDJ = djID!
              
                    Database.database().reference().child("dj_users").child(self.currentDJ).observeSingleEvent(of: .value, with: { (snapshot) in
                        
                        // Get user fullname
                        let value = snapshot.value as? [String : Any]
                        
                        if let fullname = value?["fullname"] as? String, let imageUrl = value?["image"] as? String {
                            self.currentDJNameLbl.text = fullname
                            
                            let url = URL(string: imageUrl)
                            
                            DispatchQueue.global().async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if(data != nil){
                                       self.dj_profileimg.image = UIImage(data: data!)
                                    }
                                    
                                }
                            }
                        }
                        
                    }) { (error) in
                        print(error.localizedDescription)
                    }
            
                
                // Download in memory with a maximum allowed size of 1MB (1 * 1024 * 1024 bytes)
//                Storage.storage().reference().child(self.currentDJ).getData(maxSize: 1 * 12024 * 12024) { data, error in
//                    if let error = error {
//                        // Uh-oh, an error occurred!
//                        print("Couldn't load image " + error.localizedDescription)
//                    } else {
//                        // Data for "images/island.jpg" is returned
//                        if let image = UIImage(data: data!) {
//                            self.dj_profileimg.image = image
//                        }
//                    }
//                }
                self.hasCurrentDjView.isHidden = false
                self.noCurrentDjView.isHidden = true
                
                
            }else{
                self.hasCurrentDjView.isHidden = true
                self.noCurrentDjView.isHidden = false
                
            }
            
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ChangeValue(_ sender: UIStepper) {
        
        value = sender.value
        if (value == 0){
             button_Tip.isEnabled = false
        }else{
             button_Tip.isEnabled = true
        }
        total.text = "$\(Int(value))"
        
    }
    
    @IBAction func SendTip(_ sender: Any) {
      
        if(currentDJ == "" ){
            let alert = UIAlertController(title: "Failed to send tip", message: "Please choose your live DJ first", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        //Uncomment if you have the means for testing the app using apple pay
        let paymentNetwork = [PKPaymentNetwork.amex, .chinaUnionPay, .masterCard , .visa, .discover]
        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetwork){
            paymentRequest.currencyCode = "USD"
            paymentRequest.countryCode = "US"
            paymentRequest.merchantIdentifier = "merchant.com.tuneinapp.tunein"
            paymentRequest.supportedNetworks = paymentNetwork
            paymentRequest.merchantCapabilities = .capability3DS
            paymentRequest.paymentSummaryItems  = getTotal()
        
        
        
            let applePayVC = PKPaymentAuthorizationViewController(paymentRequest:  paymentRequest)
            applePayVC?.delegate = self
        
           
            
            
            self.navigationController!.viewControllers.first!.present((applePayVC ?? nil)!, animated: true, completion: nil)
        }else{
           
        }
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let secongs = calendar.component(.second, from: date)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd_MM_yyyy"
        var result = formatter.string(from: date)
        result += ("_"+String(hour))
         result += ("_"+String(minutes))
         result += ("_"+String(secongs))
        
        
      
       print(result)
         let msg = "\(userName) has sent you $\(String(format: "%.2f", value))"
        ref.child("dj_users").child(currentDJ).child("tips").child((Auth.auth().currentUser?.uid)!).child(result).setValue(value)
        
        ref.child("dj_users").child(currentDJ).child("notifications").child("tips").child((Auth.auth().currentUser?.uid)!).child(result).setValue(msg)
        ref.child("regular_users").child((Auth.auth().currentUser?.uid)!).child("notifications").child("tips").child(currentDJ).child(result).setValue(value)
        
        
      
    }
    
     func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: ((PKPaymentAuthorizationStatus) -> Void)) {
        completion(PKPaymentAuthorizationStatus.success)
        controller.dismiss(animated: true, completion: nil)
    }
    
    
   
    
     func paymentAuthorizationViewControllerDidFinish(controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func getTotal() -> [PKPaymentSummaryItem]{
        let test = Decimal(value)
        let myInt = NSDecimalNumber.init(decimal: test)
        return [PKPaymentSummaryItem(label: "TIP" , amount: myInt)]
    }
    
    
   
}
