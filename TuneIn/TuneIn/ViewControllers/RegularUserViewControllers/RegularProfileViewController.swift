//
//  RegularProfileViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 1/9/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import Firebase

class RegularProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // MARK: Outlets
    @IBOutlet weak var button_LogOut: UIButton!
    @IBOutlet weak var imageview_ProfilePicture: UIImageView!
    @IBOutlet weak var label_ProfileName: UILabel!
    @IBOutlet weak var label_ProfileEmail: UILabel!
    @IBOutlet weak var tableview_Others: UITableView!
    @IBOutlet weak var tableviewHeightConstraint: NSLayoutConstraint!
    
    // MARK: Variables
    let otherFunctionalities = ["Edit Profile", "Following", "Notifications"]
    var email: String = ""
    var name: String = ""
    
    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        removeBorderAddShadowToNavigationBar()
        
        self.tabBarController!.tabBar.layer.borderWidth = 0.50
        self.tabBarController!.tabBar.layer.borderColor = UIColor.clear.cgColor
        self.tabBarController?.tabBar.clipsToBounds = true
        
        roundButtons(buttons: [button_LogOut])
        button_LogOut.layer.borderWidth = 1
        button_LogOut.layer.borderColor = UIColor(hexCode: "#260909").cgColor
        imageview_ProfilePicture.layer.cornerRadius = imageview_ProfilePicture.frame.height / 2
        
        //Code for getting user info moved to view will appear so its updated
       
    }
    override func viewWillAppear(_ animated: Bool) {
        // Code to get email and fullname from current user
        if let userUID = Auth.auth().currentUser?.uid, let userEmail = Auth.auth().currentUser?.email {
            label_ProfileEmail.text = userEmail
            self.email = userEmail
            Database.database().reference().child("regular_users").child(userUID).observeSingleEvent(of: .value, with: { (snapshot) in
                
                // Get user fullname
                let value = snapshot.value as? [String : Any]
                
                if let fullname = value?["fullname"] as? String, let imageUrl = value?["image"] as? String {
                    self.label_ProfileName.text = fullname
                    self.name = fullname
                    
                    let url = URL(string: imageUrl)
                    
                    DispatchQueue.global().async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if(data != nil){
                            self.imageview_ProfilePicture.image = UIImage(data: data!)
                            }
                            
                        }
                    }
                }
                
            }) { (error) in
                print(error.localizedDescription)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Conforms UITableView protocols
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return otherFunctionalities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "otherFuncCell", for: indexPath)
        cell.textLabel?.text = otherFunctionalities[indexPath.row]
        return cell
    }
    
    // Function to resize tableview as the contentsize
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    // Function to resize tableview as the contentsize
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.tableviewHeightConstraint.constant = self.tableview_Others.contentSize.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            performSegue(withIdentifier: "ToEditProfile", sender: self)
        } else if indexPath.row == 1 {
            performSegue(withIdentifier: "ToFollowing", sender: self)
        } else {
            performSegue(withIdentifier: "ToNotifications", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? RegularEditProfileViewController {
            destination.email = self.email
            destination.name = self.name
            destination.image = self.imageview_ProfilePicture.image
        }
    }
    
    // Code to help user sign out
    @IBAction func signOutTapped(_ button: UIButton) {
        do {
            try Auth.auth().signOut()
            UserDefaults.standard.removeObject(forKey: "userEmail")
            UserDefaults.standard.removeObject(forKey: "userPassword")
            performSegue(withIdentifier: "BackToBeginning", sender: self)
        } catch {
            print(error.localizedDescription)
        }
    }

}
