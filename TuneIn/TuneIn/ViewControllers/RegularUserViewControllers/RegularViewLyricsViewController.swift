//
//  RegularUserViewLyricsViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 1/18/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit

class RegularViewLyricsViewController: UIViewController {
    
    // MARK: Outlet
    @IBOutlet weak var textview_Lyrics: UITextView!
    @IBOutlet weak var view_EmptyState: UIView!
    @IBOutlet weak var process: UIActivityIndicatorView!
    
    // MARK: Variables
    var chosenTrack: Track!
    let dispatch = DispatchGroup()
    var lyrics: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        var title = chosenTrack.getName
        var artist = chosenTrack.getArtist
        title = title.replacingOccurrences(of: " ", with: "%20")
        artist = artist.replacingOccurrences(of: " ", with: "%20")
        let urlString = "https://orion.apiseeds.com/api/music/lyric/\(artist)/\(title)?apikey=yPSQuoZUV9NCxSjfjBBKbBKynIiU4KBMD9LwifGKjxmjAkuIL52HouzvuuKQlev1"
        
        downloadLyrics(jsonURL: URL(string: urlString)!)
        textview_Lyrics.text = ""
        
        dispatch.notify(queue: .main) {
            DispatchQueue.main.async {
                
                self.process.stopAnimating()
                
                if self.lyrics.count > 0 {
                    self.textview_Lyrics.isHidden = false
                    self.view_EmptyState.isHidden = true
                    self.textview_Lyrics.text = self.lyrics
                } else {
                    self.textview_Lyrics.isHidden = true
                    self.view_EmptyState.isHidden = false
                }
            }
        }
        
        removeBorderAddShadowToNavigationBar()
        process.hidesWhenStopped = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadLyrics(jsonURL: URL) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        self.dispatch.enter()
        self.process.startAnimating()
        self.process.hidesWhenStopped = false
        let task = session.dataTask(with: jsonURL) { (data, response, error) in
            
            // Bail out on error
            if error != nil { return }
            
            guard let response = response as? HTTPURLResponse, response.statusCode == 200, let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    guard let result = json["result"] as? [String: Any], let track = result["track"] as? [String: Any], let lyrics = track["text"] as? String else {
                        print("Cannot get result")
                        return
                    }
                    
                    self.lyrics = self.chosenTrack.getName + " - " + self.chosenTrack.getArtist + " lyrics\n" + lyrics
                    
                    self.dispatch.leave()
                }
            } catch {
                print(error.localizedDescription)
            }
            
            self.dispatch.wait()
            DispatchQueue.main.async {
                self.textview_Lyrics.text = self.lyrics
            }
        }
        task.resume()
        
    }
}
