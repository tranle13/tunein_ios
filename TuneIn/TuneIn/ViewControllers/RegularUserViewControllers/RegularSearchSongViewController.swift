//
//  RegularSearchSongViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 12/16/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import FirebaseDatabase
import UIKit
import FirebaseAuth
import Firebase


class RegularSearchSongViewController: UIViewController, UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableview_SongInformation: UITableView!
    @IBOutlet weak var view_NoSearchResultAndSearchSongView: UIView!
    @IBOutlet weak var imageview_Search: UIImageView!
    @IBOutlet weak var label_SearchMessage: UILabel!
    @IBOutlet weak var view_ViewLyrics: UIView!
    @IBOutlet weak var view_SubViewLyrics: UIView!
    @IBOutlet weak var button_ViewLyrics: UIButton!
    @IBOutlet weak var imageview_SongCover: UIImageView!
    @IBOutlet weak var label_SongTitle: UILabel!
    @IBOutlet weak var label_SongArtist: UILabel!
    
    var currentDJ = ""
    
    // MARK: Variables
    var ref: DatabaseReference!
    var tracks: [Track] = []
    let apiKey = "e1304e5bec6128eb33251f11404b0716"
    let dispatch = DispatchGroup()
    let process: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    var searchController = UISearchController(searchResultsController: nil)
    let noResultString = "Uh oh... I can't find anything, maybe another song?"
    let searchMessage = "Let's search for a song!"
    let noSearchResultImage = #imageLiteral(resourceName: "NoSearchResult")
    let searchMessageImage = #imageLiteral(resourceName: "SearchMessage")
    var chosenTrack: Track!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Setup a search controller
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.definesPresentationContext = true
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.definesPresentationContext = true
        
        process.center = self.view.center
        
        ref = Database.database().reference()
        // To receive updates to the searches here in this ViewController
        searchController.searchResultsUpdater = self
        
        // Setup the SearchBar
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.tintColor = UIColor.white
        searchController.searchBar.sizeToFit()
        navigationItem.searchController = searchController
        
        roundButtons(buttons: [button_ViewLyrics])
        
        let dismissGesture = UITapGestureRecognizer(target: self, action: #selector(dismissViewLyrics))
        view_ViewLyrics.addGestureRecognizer(dismissGesture)
        
        view_SubViewLyrics.layer.cornerRadius = 15
        view_SubViewLyrics.layer.shadowColor = UIColor.white.cgColor
        view_SubViewLyrics.layer.shadowRadius = 8
        view_SubViewLyrics.layer.shadowOffset = CGSize(width: -1, height: 1)
        view_SubViewLyrics.layer.shadowOpacity = 0.8
        
        imageview_Search.image = UIImage(named: "empty_state_1")
        imageview_Search.alpha = 0.5
        label_SearchMessage.text = searchMessage
        
        self.view.addSubview(process)
        process.bringSubview(toFront: self.view)
        
        ref = Database.database().reference()
        let userID = Auth.auth().currentUser?.uid
        
        ref.child("regular_users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let djID = value?["currentLiveDJ"] as? String
            
            
            if djID != nil{
                //We have a DJ they can select a song
                self.currentDJ = djID!
            }
        }) { (error) in
            print(error.localizedDescription)
        }
        
        self.navigationController?.navigationBar.layer.shadowOpacity = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Searchbar stubs
    // Update result based on search term
    func updateSearchResults(for searchController: UISearchController) {
        // There is no code to implement here
    }
    
    // Function to remove special characters from search term
    func removeSpecialCharsFromString(text: String) -> String {
        let validChars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890")
        return text.filter {validChars.contains($0) }
    }
    
    // Code to get search result from API
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.tracks.removeAll()
        self.view_ViewLyrics.isHidden = true
        
        // Get the search term from search bar
        var searchText = searchController.searchBar.text!
        searchText = removeSpecialCharsFromString(text: searchText)
        let trimmedSearchTerm = searchText.replacingOccurrences(of: " ", with: "")
        
        if !trimmedSearchTerm.isEmpty {
            
            let finalSearchText = searchText.replacingOccurrences(of: " ", with: "%20")
            let urlString = "https://ws.audioscrobbler.com/2.0/?method=track.search&track=\(finalSearchText)&api_key=e1304e5bec6128eb33251f11404b0716&format=json"
            
            downloadAndParse(jsonURL: URL(string: urlString)!, searchTerm: finalSearchText)
            
            dispatch.notify(queue: .main) {
                DispatchQueue.main.async {
                    self.process.hidesWhenStopped = true
                    self.process.stopAnimating()
                    
                    if self.tracks.count > 0 {
                        self.view_NoSearchResultAndSearchSongView.isHidden = true
                        self.tableview_SongInformation.isHidden = false
                    } else {
                        self.label_SearchMessage.text = self.noResultString
                        self.view_NoSearchResultAndSearchSongView.isHidden = false
                        self.tableview_SongInformation.isHidden = true
                        
                    }
                    
                    self.tableview_SongInformation.reloadData()
                }
            }
        }
    }
    
    // Display the search message when user cancels searches
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        imageview_Search.image = UIImage(named: "empty_state_1")
        label_SearchMessage.text = searchMessage
        self.view_NoSearchResultAndSearchSongView.isHidden = false
        self.tableview_SongInformation.isHidden = true
        self.view_ViewLyrics.isHidden = true
        
        process.stopAnimating()
        process.hidesWhenStopped = true
    }
    
    // MARK: UITableViewDelegate & UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as? CustomTrackTableCell else {
            return tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath)
        }
        
        if indexPath.row < tracks.count {
            let track = tracks[indexPath.row]
            cell.imageview_TrackImage.image = track.getCover
            cell.label_TrackName.text = track.getName
            cell.label_TrackArtist.text = track.getArtist
            cell.button_SendRequest.tag = indexPath.row
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        chosenTrack = tracks[indexPath.row]
        imageview_SongCover.image = chosenTrack.getCover
        label_SongTitle.text = chosenTrack.getName
        label_SongArtist.text = chosenTrack.getArtist
        
        UIView.animate(withDuration: 1.5, delay: 0, options: .curveEaseOut, animations: {
            self.view_ViewLyrics.alpha = 1
        }) { (result) in
            self.view_ViewLyrics.isHidden = false
        }
    }
    
    @IBAction func viewLyricsTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "ToViewLyrics", sender: self)
    }
    
    @objc func dismissViewLyrics() {
        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseOut, animations: {
            self.view_ViewLyrics.alpha = 0
        }) { (result) in
            self.view_ViewLyrics.isHidden = true
        }
    }
    
    // Function to send song request to DJ
    @IBAction func SendSongRequest(_ sender: UIButton) {
      
        if currentDJ == "" {
            let alert = UIAlertController(title: "Failed to send request", message: "Please choose your live DJ first", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }

        self.ref.child("dj_users").child(currentDJ).child("requested_songs").child(Auth.auth().currentUser?.uid ?? "test").setValue(tracks[sender.tag].getName)
        
        self.ref.child("regular_users").child(Auth.auth().currentUser?.uid ?? "" ).child("requested_song").child("title").setValue(tracks[sender.tag].getName)
        self.ref.child("regular_users").child(Auth.auth().currentUser?.uid ?? "" ).child("requested_song").child("artist").setValue(tracks[sender.tag].getArtist)
        self.ref.child("regular_users").child(Auth.auth().currentUser?.uid ?? "" ).child("requested_song").child("url").setValue(tracks[sender.tag].getURL)
        
        let alert = UIAlertController(title: "Requested Song", message: "Successfully requested your song! Requesting another song will replace this.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // Function to pass chosen song information to the next screen to view lyrics
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? RegularViewLyricsViewController {
            destination.chosenTrack = self.chosenTrack
        }
    }
    
}
