//
//  RegularRegisterViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 12/4/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging

class RegularRegisterViewController: UIViewController, UITextFieldDelegate {

    // MARK: Outlets
    @IBOutlet weak var btn_Register: UIButton!
    @IBOutlet weak var txt_FullName: CustomTextField!
    @IBOutlet weak var txt_Email: CustomTextField!
    @IBOutlet weak var txt_Password: CustomTextField!
    @IBOutlet weak var txt_ConfirmPassword: CustomTextField!
    @IBOutlet weak var swt_ApplePaySwitch: UISwitch!
    @IBOutlet weak var btn_Facebook: UIButton!
    @IBOutlet weak var btn_Instagram: UIButton!
    
    // MARK: Variable
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Rounded the custom TextFields
        for textfield in [txt_Email, txt_Password, txt_FullName, txt_ConfirmPassword] {
            textfield?.layer.cornerRadius = (textfield?.frame.height)! / 2 - 10
            textfield?.delegate = self
        }
        
        // Rounded Sign in, Register buttons and give them shadow
        roundButtons(buttons: [btn_Register])
        
        // Set content mode of button's image view to scaleAspectFit
        btn_Facebook.imageView?.contentMode = .scaleAspectFit
        btn_Instagram.imageView?.contentMode = .scaleAspectFit
        
        ref = Database.database().reference()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Sign up for new account when conditions are satisfied
    @IBAction func register(_ sender: UIButton) {
        
        // Check if all textfields are filled
        if !(txt_FullName.text?.isEmpty)!, !(txt_FullName.text?.isEmpty)!, !(txt_Password.text?.isEmpty)!, !(txt_ConfirmPassword.text?.isEmpty)! {
            
            if (txt_FullName.text?.count)! > 3 {
                if checkPassword(password: txt_Password.text!) {
                    // Check if password and confirm password match
                    if txt_Password.text! == txt_ConfirmPassword.text! {
                        // Register user with Firebase Authentication
                        Auth.auth().createUser(withEmail: self.txt_Email.text!, password: self.txt_Password.text!, completion: { (authResults, error) in
                            
                            if let user = authResults?.user {
                                self.ref.child("regular_users").child(user.uid).child("fullname").setValue(self.txt_FullName.text!)
                                self.ref.child("regular_users").child(user.uid).child("email").setValue(self.txt_Email.text!)
                                self.ref.child("regular_users").child(user.uid).child("isDJ").setValue(false)
                                let token : [String: AnyObject] = [Messaging.messaging().fcmToken!: Messaging.messaging().fcmToken as AnyObject]
                                self.postToken(Token: token)
                                UserDefaults.standard.set(self.txt_Email.text!, forKey: "userEmail")
                                UserDefaults.standard.set(self.txt_Password.text!, forKey: "userPassword")
                                
                                let storageRef = Storage.storage().reference().child(Auth.auth().currentUser?.uid ?? "default value")
                                if let image = UIImage(named: "EmptyProfilePicture"), let resizedImage = image.resized(withPercentage: 200/image.size.width), let finalImage = UIImagePNGRepresentation(resizedImage) {
                                    storageRef.putData(finalImage, metadata: nil, completion: { (metadata, error) in
                                        if error != nil {
                                            self.createAlert(message: error?.localizedDescription)
                                        } else {
                                            storageRef.downloadURL(completion: { (url, error) in
                                                if error != nil {
                                                    self.createAlert(message: error?.localizedDescription)
                                                } else {
                                                    if let url = url?.absoluteString {
                                                        Database.database().reference().child("regular_users").child(user.uid).child("image").setValue(url)
                                                        self.performSegue(withIdentifier: "ToHomeScreen2", sender: self)
                                                    }
                                                }
                                            })
                                        }
                                        
                                    })
                                }
                            } else {
                                self.createAlert(message: error?.localizedDescription)
                            }
                        })
                    } else {
                        createAlert(message: "Your passwords don't match. Please check it again")
                    }
                } else {
                    createAlert(message: "Your password needs to have at least one uppercased letter, at least one lowercased letter, at least a number and at least a special character")
                }
            } else {
                self.createAlert(message: "Your fullname needs to have more than 3 characters")
            }
        
        // If some of textfields are empty, inform user with alert
        } else {
            createAlert(message: nil)
        }
    }
    
    // Code to dismiss the keyboard when user touches the screen
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    // Code to dismiss the current view to the previous view
    @IBAction func dismissView(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // Code to go to next textfields and sign up when user taps on Go button on keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    // Code to display alert about Apple Pay permission
    @IBAction func switchOnApplePay(_ sender: UISwitch) {
        switchOnApplePay(applePaySwitch: sender)
    }
    func postToken(Token: [String: AnyObject]){
        let dbRef = Database.database().reference()
        dbRef.child("fcmToken").child((Auth.auth().currentUser?.uid ?? "thisisnotright")).setValue(Token)
        
    }
}
