//
//  RegularEditProfileViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 1/9/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import Firebase
import FirebaseAuth
import UIKit
import AVFoundation

class RegularEditProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: Outlets
    @IBOutlet weak var imageview_ProfileImage: UIImageView!
    @IBOutlet weak var textfield_FullName: CustomTextField!
    @IBOutlet weak var textfield_Email: CustomTextField!
    
    // MARK: Variables
    var name: String!
    var email: String!
    var image: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imageview_ProfileImage.layer.cornerRadius = imageview_ProfileImage.frame.height / 2 + 1
        
        textfield_FullName.text = name
        textfield_Email.text = email
        
        if image != nil {
            imageview_ProfileImage.image = image
        }
        
        self.tabBarController?.tabBar.isHidden = true
    }
    
    // Code to show tab bar again after the view is dismissed
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Function to save changes to profile
    @IBAction func saveEditInformation(_ sender: UIBarButtonItem) {
        
        var ref: DatabaseReference!
        ref = Database.database().reference()
        let userID = Auth.auth().currentUser?.uid
        if((textfield_Email.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! || (textfield_FullName.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)!){
            let alert = UIAlertController(title: "Update profile", message: "You can not have a empty name or email!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        if !textfield_Email.text!.isEmpty, !textfield_FullName.text!.isEmpty {
            if textfield_Email.text != email && isValidEmail(testStr: textfield_Email.text!) {
                Auth.auth().currentUser?.updateEmail(to: textfield_Email.text ?? "") { (error) in
                   
                    if (error == nil){
                        self.email = self.textfield_Email.text
                    ref.child("regular_users").child(userID!).child("email").setValue(self.textfield_Email.text)
                    }
                }
                
            }
            
            if self.textfield_Email.text != name {
                name = self.textfield_FullName.text
                let userID = Auth.auth().currentUser?.uid
                ref.child("regular_users").child(userID!).child("fullname").setValue(self.textfield_FullName.text)
            }
            
            if imageview_ProfileImage.image != image {
                image = imageview_ProfileImage.image
                let storageRef = Storage.storage().reference().child(Auth.auth().currentUser?.uid ?? "default value")
                if let image = self.imageview_ProfileImage.image, let resizedImage = image.resized(withPercentage: 200/image.size.width), let uploadImage = UIImagePNGRepresentation(resizedImage) {
                    storageRef.putData(uploadImage, metadata: nil, completion: { (metadata, error) in
                        if error != nil {
                            self.createAlert(message: error?.localizedDescription)
                        } else {
                        
                            storageRef.downloadURL(completion: { (url, error) in
                                if error != nil {
                                    self.createAlert(message: error?.localizedDescription)
                                } else {
                                    if let url = url?.absoluteString {
                                        Database.database().reference().child("regular_users").child(userID!).child("image").setValue(url)
                                    }
                                }
                            })
                        }
                        
                    })
                }
            }
            
            let alert = UIAlertController(title: "Update profile", message: "Saved changes to your profile successfully", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            self.createAlert(message: nil)
        }
    }
    
    // Code to dismiss keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
      
    }
    
    // Code to check if entered email is valid
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @IBAction func changeProfileImageTapped(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: "Change profile picture", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.checkAuthorization(type: .camera)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            self.checkAuthorization(type: .photoLibrary)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // Function to present Camera if permission is granted
    func presentPhotoAndCamera(type: UIImagePickerControllerSourceType) {
        let pickerController = UIImagePickerController()
        if type == .camera {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                pickerController.sourceType = .camera
            }
        } else {
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                pickerController.sourceType = .photoLibrary
            }
        }
        
        pickerController.delegate = self
        self.present(pickerController, animated: true, completion: nil)
    }
    
    // Function to request for camera's permission when the status is undetermined
    func requestPhotoPermission(type: UIImagePickerControllerSourceType) {
        AVCaptureDevice.requestAccess(for: .video) { (granted) in
            guard granted == true else { return }
            self.presentPhotoAndCamera(type: type)
        }
    }
    
    // Function to display an alert notifying user to allow Camera in Settings
    func alertCameraAccessNeeded(type: UIImagePickerControllerSourceType) {
        let settingsAppUrl = URL(string: UIApplicationOpenSettingsURLString)!
        
        let alert = UIAlertController(title: "Camera Access", message: "Tune-In does not have access to your camera. Please go to Settings and turn on Camera", preferredStyle: .alert)
        
        if type == .photoLibrary {
            alert.message = "Tune-In does not have access to your photo library. Please go to Settings and allow access to Photos"
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel, handler: { (action) in
            UIApplication.shared.open(settingsAppUrl, options: [:], completionHandler: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // Check for camera authorization
    func checkAuthorization(type: UIImagePickerControllerSourceType) {
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        switch status {
            
        // If user already authorizes it, open the camera
        case .authorized:
            self.presentPhotoAndCamera(type: type)
            
        // If user hasn't, ask for permission again
        case .notDetermined:
            self.requestPhotoPermission(type: type)
            
        // If user has denied access before
        case .denied, .restricted:
            alertCameraAccessNeeded(type: type)
        }
    }
    
    // Function to get taken image from camera or chosen one from photo library
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        guard let image1 = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            print("No image found")
            return
        }
        picker.dismiss(animated: true, completion: nil)
        imageview_ProfileImage.image = image1.resized(withPercentage: 200/image1.size.width)
    
    }

}
extension UIImage {
    // MARK: - UIImage+Resize
    func compressTo(_ expectedSizeInMb:Int) -> UIImage? {
        let sizeInBytes = expectedSizeInMb * 1024 * 1024
        var needCompress:Bool = true
        var imgData:Data?
        var compressingValue:CGFloat = 1.0
        while (needCompress && compressingValue > 0.0) {
            if let data:Data = UIImageJPEGRepresentation(self, compressingValue) {
                if data.count < sizeInBytes {
                    needCompress = false
                    imgData = data
                } else {
                    compressingValue -= 0.1
                }
            }
        }
        
        if let data = imgData {
            if (data.count < sizeInBytes) {
                return UIImage(data: data)
            }
        }
        return nil
    }
}
