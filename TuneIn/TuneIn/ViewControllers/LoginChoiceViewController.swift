//
//  LoginChoiceViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 1/16/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import Firebase

class LoginChoiceViewController: UIViewController {
    
    // MARK: Variables
    var credential: AuthCredential!

    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Function to add information to Firebase and go to DJ home tab
    @IBAction func toDJHomeTapped(_ sender: UIButton) {
        if credential != nil {
            Auth.auth().signInAndRetrieveData(with: credential, completion: { (result, error) in
                if error != nil {
                    print((error?.localizedDescription)!)
                } else {
                    if let uid = Auth.auth().currentUser?.uid {
                        let ref = Database.database().reference().child("dj_users").child(uid)
                        ref.child("fullname").setValue(result?.user.displayName)
                        ref.child("email").setValue(result?.user.email)
                        ref.child("isDJ").setValue(true)
                        
                         self.performSegue(withIdentifier: "ToDjHome", sender: self)
                      
                        
                        
                        
                       
                    }
                }
            })
        }
    }
    
    // Function to add information to Firebase and go to audience home tab
    @IBAction func toAudienceHomeTapped(_ sender: UIButton) {
        if credential != nil {
            Auth.auth().signInAndRetrieveData(with: credential, completion: { (result, error) in
                if error != nil {
                    print((error?.localizedDescription)!)
                } else {
                    if let uid = Auth.auth().currentUser?.uid {
                        let ref = Database.database().reference().child("regular_users").child(uid)
                        ref.child("fullname").setValue(result?.user.displayName)
                        ref.child("email").setValue(result?.user.email)
                        ref.child("isDJ").setValue(false)
                        ref.child("isLive").setValue(false)
                        
                        self.performSegue(withIdentifier: "ToAudienceHome", sender: self)
                        
                       
                    }
                }
            })
        }
    }
    
    // Code for user to dismiss view
    @IBAction func dismissView(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
