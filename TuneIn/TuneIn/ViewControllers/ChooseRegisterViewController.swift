//
//  ChooseRegisterViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 12/4/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import UIKit

class ChooseRegisterViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var btn_Yes: UIButton!
    @IBOutlet weak var btn_No: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        roundButtons(buttons: [btn_Yes, btn_No])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Function to dismiss view
    @IBAction func dismissView() {
        dismiss(animated: true, completion: nil)
    }
}
