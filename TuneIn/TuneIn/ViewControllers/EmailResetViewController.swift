//
//  EmailResetViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 12/5/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import UIKit
import Firebase
import MessageUI

class EmailResetViewController: UIViewController, MFMailComposeViewControllerDelegate {

    // MARK: Outlets
    @IBOutlet weak var txt_Email: CustomTextField!
    @IBOutlet weak var btn_Send: UIButton!
    
    // MARK: Variables
    var emailString: String?
    var fromWhere: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        roundButtons(buttons: [btn_Send])
        
        if let emailString = emailString {
            txt_Email.text = emailString
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Functions
    @IBAction func sendVerificationTapped(_ sender: UIButton) {
        if checkTextField(texts: [txt_Email.text!]) {
            
            // If the email textfield is not empty, send a verification link in an email to the user's email address
            Auth.auth().sendPasswordReset(withEmail: txt_Email.text!) { (error) in
                if error != nil {
                    let feedback = UIAlertController(title: "Error", message: error?.localizedDescription ?? "Please check your internet connection again", preferredStyle: .alert)
                    feedback.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(feedback, animated: true, completion: nil)
                } else {
                    let feedback = UIAlertController(title: "Email Sent!", message: nil, preferredStyle: .alert)
                    feedback.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(feedback, animated: true, completion: nil)
                }
            }
            
        // If the email textfield is empty, inform user with alert
        } else {
            createAlert(message: nil)
        }
    }
    
    // Code to dismiss the view
    @IBAction func dismissView(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // Code to bring the email to the EmailResetViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? EmailResetViewController {
            destination.emailString = txt_Email.text!
        }
    }
    
}
