//
//  DJGenreViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 12/7/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import UIKit

class DJGenreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    // MARK: Outlets
    @IBOutlet weak var button_CheckMarkDone: UIButton!
    @IBOutlet weak var tableview_Genres: UITableView!
    @IBOutlet weak var textfield_OtherGenres: CustomTextField!
    
    // MARK: Variable
    let genres: [String] = ["Ambient", "Bass", "Breakbeat", "Disco", "Drum 'n Bass", "Dubstep", "Electro", "Funk", "Heavy metal", "Hip hop & rap", "House", "Pop", "Punk rock", "Reggae", "Rock", "Techno", "Trance", "Trap"]
    var allGenres: [Genre] = []
    var selectedGenres: [String] = []
    var genreString: String = ""
    var tapGesture: UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        button_CheckMarkDone.imageView?.contentMode = .scaleAspectFit
        
        // Populate the state for genre
        for genre in genres {
            allGenres.append(Genre(name: genre, state: false))
        }
        
        tableview_Genres.separatorColor = UIColor.white
        
        // Notify the UI when keyboard appears so that the whole view can be raised up
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // Create tap gesture to dismiss the keyboard when user taps on the textfield for custom genre
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Conform UITableView's protocol stubs
    // Function to tell tableview how many cell we needs
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allGenres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "genreCell", for: indexPath) as? CustomGenreTableCell else {
            return tableView.dequeueReusableCell(withIdentifier: "genreCell", for: indexPath)
        }
        
        if allGenres[indexPath.row].getGenreState {
            cell.button_CheckMark.setImage(#imageLiteral(resourceName: "CheckedBox"), for: .normal)
        } else {
            cell.button_CheckMark.setImage(UIImage(), for: .normal)
        }
        
        cell.label_Genre.text = genres[indexPath.row]
        
        return cell
    }
    
    // Code to change the genre state so that UI will be updated to show user that he/she selects something
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentState = allGenres[indexPath.row].getGenreState
        allGenres[indexPath.row].getGenreState = !currentState
        
        if !currentState == true {
            selectedGenres.append(allGenres[indexPath.row].getGenreName)
        } else {
            if let index = selectedGenres.index(of: allGenres[indexPath.row].getGenreName) {
                selectedGenres.remove(at: index)
            }
        }
        
        tableview_Genres.reloadData()
    }
    
    // Function to raise the view up when keyboard appears
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        guard let keyboardSize = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue else { return }
        let keyboardFrame = keyboardSize.cgRectValue
        if self.view.frame.origin.y == 0 {
            self.view.frame.origin.y -= keyboardFrame.height
        }
        
        tableview_Genres.addGestureRecognizer(tapGesture)
    }
    
    // Function to drag the view down when keyboard disappears
    @objc func keyboardWillHide(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        guard let keyboardSize = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue else { return }
        let keyboardFrame = keyboardSize.cgRectValue
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y += keyboardFrame.height
        }
        
        tableview_Genres.removeGestureRecognizer(tapGesture)
    }
    
    // Function to dismiss keyboard when user taps on tableview
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
        self.view.frame.origin.y = 0
    }
    
    // Function to dismiss the keyboard when the user touches on the self.view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    // MARK: IBAction functions
    // Function to dismiss view if user taps on x button
    @IBAction func dismissView(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // Function to check if user chooses any genres and make a complete out of the chosen and custom genres. If user does then let them go back to the previous screen and if he/she doesn't then don't let them go back with the check mark button
    @IBAction func checkMarkButtonTapped(_ sender: UIButton) {
        let customGenre = textfield_OtherGenres.text!
        let trimmedCustomGenre = customGenre.replacingOccurrences(of: " ", with: "")
        let superTrimmedCustomGenre = trimmedCustomGenre.replacingOccurrences(of: ",", with: "")
        if selectedGenres.count > 0 || superTrimmedCustomGenre.count > 0 {
            
            let customGenres = customGenre.components(separatedBy: ",")
            
                if superTrimmedCustomGenre.count > 0 {
                for customKind in customGenres {
                    var kind = customKind
                    while kind.first == " " {
                        kind.removeFirst()
                    }
                    
                    while kind.last == " " {
                        kind.removeLast()
                    }
                    
                    selectedGenres.append(kind)
                }
            }
            
            for (offset, genre) in selectedGenres.enumerated() {
                if offset != selectedGenres.count - 1 {
                    genreString.append("\(genre), ")
                } else {
                    genreString.append(genre)
                }
            }
            
            performSegue(withIdentifier: "BackToDJRegistration", sender: self)
        } else {
            let alert = UIAlertController(title: "No genres selected", message: "Please select one or more genres or fill in approrpiate custom genres into the text box below", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return false
    }
}
