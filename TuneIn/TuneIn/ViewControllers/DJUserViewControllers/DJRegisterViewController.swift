//
//  DJRegisterViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 12/4/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation

class DJRegisterViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    // MARK: Outlets
    @IBOutlet weak var imageview_ProfileImage: UIImageView!
    @IBOutlet weak var textfield_FullName: CustomTextField!
    @IBOutlet weak var textfield_Email: CustomTextField!
    @IBOutlet weak var textfield_Password: CustomTextField!
    @IBOutlet weak var textfield_ConfirmPassword: CustomTextField!
    @IBOutlet weak var textfield_MusicGenre: CustomTextField!
    @IBOutlet weak var switch_ApplePay: UISwitch!
    @IBOutlet weak var button_Register: UIButton!
    @IBOutlet weak var button_Facebook: UIButton!
    @IBOutlet weak var button_Instagram: UIButton!
    @IBOutlet weak var button_EditImage: UIButton!
    
    // MARK: Variable
    let ref = Database.database().reference()
    var genreString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        button_Register.layer.cornerRadius = button_Register.frame.height / 2 - 10
        button_Register.layer.shadowColor = UIColor.black.cgColor
        button_Register.layer.shadowRadius = 5
        button_Register.layer.shadowOffset = CGSize(width: 0, height: 3)
        button_Register.layer.shadowOpacity = 0.32
        
        imageview_ProfileImage.layer.cornerRadius = imageview_ProfileImage.frame.height / 2
        button_EditImage.layer.cornerRadius = imageview_ProfileImage.frame.height / 2
        
        button_Facebook.imageView?.contentMode = .scaleAspectFit
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: IBAction funtions
    // Code to dismiss the current view when user taps on the x button
    @IBAction func dismissView(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // Code to register DJ to database
    @IBAction func register(_ sender: UIButton) {
        if !(textfield_FullName.text?.isEmpty)!, !(textfield_Email.text?.isEmpty)!, !(textfield_Password.text?.isEmpty)!, !(textfield_ConfirmPassword.text?.isEmpty)!, !(textfield_MusicGenre.text?.isEmpty)! {
            
            if (textfield_FullName.text?.count)! > 3 {
                if checkPassword(password: textfield_Password.text!) {
                
                    // Check if password and confirm password match
                    if textfield_Password.text! == textfield_ConfirmPassword.text! {
                        
                        // Display popup saying that developers would take 15% from each transaction
                        let feedback = UIAlertController(title: nil, message: "Please acknowledge that we would take 15% from each transaction from the audience. Refusing this means you cannot create an account", preferredStyle: .alert)
                        
                        
                        // Create an account for DJ user once they accept the condition
                        feedback.addAction(UIAlertAction(title: "Accept", style: .cancel, handler: { (action) in
                            Auth.auth().createUser(withEmail: self.textfield_Email.text!, password: self.textfield_Password.text!, completion: { (result, error) in
                                if error == nil, let user = result?.user {
                                    self.ref.child("dj_users").child(user.uid).child("fullname").setValue(self.textfield_FullName.text!)
                                    self.ref.child("dj_users").child(user.uid).child("email").setValue(self.textfield_Email.text!)
                                    self.ref.child("dj_users").child(user.uid).child("isDJ").setValue(true)
                                    self.ref.child("dj_users").child(user.uid).child("isLive").setValue(false)
                                    self.ref.child("dj_users").child(user.uid).child("genres").setValue(self.genreString)
                                    
                                    let token : [String: AnyObject] = [Messaging.messaging().fcmToken!: Messaging.messaging().fcmToken as AnyObject]
                                    self.postToken(Token: token)
                                    
                                    if let image = self.imageview_ProfileImage.image, let resizedImage = image.resized(withPercentage: 200/image.size.width) {
                                        self.uploadAndGetDownloadUrl(uid: user.uid, image: resizedImage, emailTextField: self.textfield_Email, passwordTextField: self.textfield_Password, isDJ: true)
                                    }
                                } else {
                                    self.createAlert(message: error?.localizedDescription)
                                }
                            })
                        }))
                        
                        // If user refuses to accept the condition, display another popup saying that they can't sign up if they don't agree
                
                        feedback.addAction(UIAlertAction(title: "Refuse", style: .default, handler: nil))
                        
                        self.present(feedback, animated: true, completion: nil)
                    } else {
                        self.createAlert(message: "Your passwords don't match. Please check it again")
                    }
                } else {
                    createAlert(message: "Your password needs to have at least one uppercased letter, at least one lowercased letter and at least a number")
                }
            } else {
                self.createAlert(message: "Your fullname needs to have more than 3 characters")
            }
        } else {
            createAlert(message: nil)
        }
    }
    
    // Function to change profile image
    @IBAction func changeProfileImage(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: "Change profile picture", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.checkAuthorization(type: .camera)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            self.checkAuthorization(type: .photoLibrary)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // Display popup asking for permission to access Apple Pay
    @IBAction func allowApplePay(_ sender: UISwitch) {
        switchOnApplePay(applePaySwitch: sender)
    }
    
    // MARK: Functions
    // Function to present Camera if permission is granted
    func presentPhotoAndCamera(type: UIImagePickerControllerSourceType) {
        let pickerController = UIImagePickerController()
        if type == .camera {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                pickerController.sourceType = .camera
            }
        } else {
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                pickerController.sourceType = .photoLibrary
            }
        }
        
        pickerController.delegate = self
        self.present(pickerController, animated: true, completion: nil)
    }
    
    // Function to request for camera's permission when the status is undetermined
    func requestPhotoPermission(type: UIImagePickerControllerSourceType) {
        AVCaptureDevice.requestAccess(for: .video) { (granted) in
            guard granted == true else { return }
            self.presentPhotoAndCamera(type: type)
        }
    }
    
    // Function to display an alert notifying user to allow Camera in Settings
    func alertCameraAccessNeeded(type: UIImagePickerControllerSourceType) {
        let settingsAppUrl = URL(string: UIApplicationOpenSettingsURLString)!
        
        let alert = UIAlertController(title: "Camera Access", message: "Tune-In does not have access to your camera. Please go to Settings and turn on Camera", preferredStyle: .alert)
        
        if type == .photoLibrary {
            alert.message = "Tune-In does not have access to your photo library. Please go to Settings and allow access to Photos"
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel, handler: { (action) in
            UIApplication.shared.open(settingsAppUrl, options: [:], completionHandler: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // Check for camera authorization
    func checkAuthorization(type: UIImagePickerControllerSourceType) {
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        switch status {
            
        // If user already authorizes it, open the camera
        case .authorized:
            self.presentPhotoAndCamera(type: type)
            
        // If user hasn't, ask for permission again
        case .notDetermined:
            self.requestPhotoPermission(type: type)
            
        // If user has denied access before
        case .denied, .restricted:
            alertCameraAccessNeeded(type: type)
        }
    }
    
    // Function to get taken image from camera or chosen one from photo library
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            print("No image found")
            return
        }
        picker.dismiss(animated: true, completion: nil)
        imageview_ProfileImage.image = image
    }
    
    // Function to get all the selected genres from DJGenreViewController
    @IBAction func rewindFromGenre(segue: UIStoryboardSegue) {
        if let source = segue.source as? DJGenreViewController {
            genreString = source.genreString
            
            if genreString.count > 0 {
                textfield_MusicGenre.text = genreString
            }
        }
    }
    
    // Function to navigate to GenreViewController if user taps on Genre textfield
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            performSegue(withIdentifier: "ToGenreBoard", sender: self)
            return false
        }
        return true
    }
    
    // Code to dismiss the keyboard when user touches the screen
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    // Code to go to next textfields and sign in when user clicks Go on keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func postToken(Token: [String: AnyObject]){
        let dbRef = Database.database().reference()
        dbRef.child("fcmToken").child((Auth.auth().currentUser?.uid)!).setValue(Token)
        
    }
    
}

extension UIImage {
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/(size.width * size.height))))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
