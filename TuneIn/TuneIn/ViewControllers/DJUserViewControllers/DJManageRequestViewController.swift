//
//  AlphaRequestedSongsTableViewController.swift
//  TuneIn
//
//  Created by Brandon Anderson on 12/16/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import UIKit
import Firebase
class DJManageRequestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // MARK: Outlet
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var view_EmptyState: UIView!
    
    // MARK: Variables
    var ref: DatabaseReference!
    var keys = [String]()
    var songs = [String]()
    var artists = [String]()
    
    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        removeBorderAddShadowToNavigationBar()

         ref = Database.database().reference()
        
        ref = Database.database().reference().child("dj_users").child(Auth.auth().currentUser?.uid ?? "").child("requested_songs")
        ref.observe(.value, with: { (snapshot) in
            self.songs.removeAll()
            self.artists.removeAll()
            self.keys.removeAll()
            guard let shotValue = snapshot.value as? [String: Any] else {
                return
            }
            
            shotValue.forEach({ (key, value) in
              
                
                for (key,value) in shotValue {
                    self.keys.append(key)
                    print("\(key) = \(value)")
                  
                    
                    self.songs.append((value as? String)!)
                       
                    }
                   
                
                
            })
            self.table_view.reloadData()
        })
    }

    // MARK: Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if songs.count > 0 {
            view_EmptyState.isHidden = true
        } else {
            view_EmptyState.isHidden = false
        }
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AlphaRequestedSongsCell", for: indexPath) as? RequestedSongsTableViewCell else {
            return tableView.dequeueReusableCell(withIdentifier: "AlphaRequestedSongsCell", for: indexPath)
        }

        cell.button_accept.tag = indexPath.row
        cell.button_reject.tag = indexPath.row
        cell.label_Title.text = songs[indexPath.row]

        return cell
    }
    
    // Function to reject song when reject button is tapped
    @IBAction func RejectSong(_ sender: UIButton) {
        let userID = Auth.auth().currentUser?.uid
         ref = Database.database().reference()
        self.ref.child("dj_users").child(userID ?? "").child("requested_songs").child(self.keys[sender.tag]).removeValue()
        
        songs.remove(at: sender.tag)
        table_view.reloadData()
    }
    
    // Function to accept song when accept button is tapped
    @IBAction func AcceptSong(_ sender: UIButton) {
         let userID = Auth.auth().currentUser?.uid
        
        ref = Database.database().reference()
        self.ref.child("dj_users").child(userID ?? "").child("approved_songs").child(keys[sender.tag]).setValue(songs[sender.tag])
        self.ref.child("dj_users").child(userID ?? "").child("requested_songs").child(self.keys[sender.tag]).removeValue()

        songs.remove(at: sender.tag)
        table_view.reloadData()
    }
    
}
