//
//  DJGenreEditViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 1/13/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import Firebase

class DJGenreEditViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: Outlets
    @IBOutlet weak var tableview_Genres: UITableView!
    @IBOutlet weak var button_AddNewGenres: UIButton!
    @IBOutlet weak var view_EmptyState: UIView!
    
    // MARK: Variables
    var trueGenres: [Genre] = []
    var selectedGenres: [String] = []
    var ref: DatabaseReference!

    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        roundButtons(buttons: [button_AddNewGenres])

        // Do any additional setup after loading the view.
        ref = Database.database().reference().child("dj_users").child(Auth.auth().currentUser?.uid ?? "").child("genres")
        
        self.tabBarController?.tabBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let uid = Auth.auth().currentUser?.uid {
            ref = Database.database().reference().child("dj_users").child(uid).child("genres")
            
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                if let genre = snapshot.value as? String {
                    self.trueGenres.removeAll()
                    let allGenres = genre.components(separatedBy: ",")
                    
                    for customKind in allGenres {
                        var kind = customKind
                        while kind.first == " " {
                            kind.removeFirst()
                        }
                        
                        while kind.last == " " {
                            kind.removeLast()
                        }
                        
                        self.trueGenres.append(Genre(name: kind, state: false))
                    }
                    
                    self.tableview_Genres.reloadData()
                    for item in self.trueGenres {
                        print(item.getGenreName)
                    }
                }
            }) { (error) in
                print(error.localizedDescription)
            }
        }
        self.tabBarController?.tabBar.isHidden = true
    }
    
    // Function to delete selected genres
    @IBAction func deleteSelectedGenres(_ sender: UIBarButtonItem) {
        if selectedGenres.count > 0 {
            let alert = UIAlertController(title: "Confirm Deletion", message: "Are you sure you want to delete all selected genres?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
                let substitute = self.trueGenres
                self.trueGenres.removeAll()
                
                for genre in substitute {
                    if !self.selectedGenres.contains(genre.getGenreName) {
                        self.trueGenres.append(Genre(name: genre.getGenreName, state: false))
                    }
                }
                
                var genreString = ""
                for (offset, genre) in self.trueGenres.enumerated() {
                    if offset != self.trueGenres.count - 1 {
                        genreString.append("\(genre.getGenreName), ")
                    } else {
                        genreString.append(genre.getGenreName)
                    }
                }
                
                self.ref.setValue(genreString)
                self.selectedGenres.removeAll()
                self.tableview_Genres.reloadData()
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // Code to show navigation bar and tab bar after the view disappears
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
    }
    
    // MARK: Conforms UITableView's protocol stubs
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if trueGenres.count > 0 {
            view_EmptyState.isHidden = true
        } else {
            view_EmptyState.isHidden = false
        }
        
        return trueGenres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "djGenreCell", for: indexPath) as? CustomGenreTableCell else {
            return tableView.dequeueReusableCell(withIdentifier: "djGenreCell", for: indexPath)
        }
        
        if trueGenres[indexPath.row].getGenreState {
            cell.button_CheckMark.setImage(#imageLiteral(resourceName: "CheckedBox"), for: .normal)
        } else {
            cell.button_CheckMark.setImage(UIImage(), for: .normal)
        }
        
        cell.button_CheckMark.layer.borderWidth = 1
        cell.button_CheckMark.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.label_Genre.text = trueGenres[indexPath.row].getGenreName
        
        return cell
    }
    
    // Function to get selected genres for deletion
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentState = trueGenres[indexPath.row].getGenreState
        trueGenres[indexPath.row].getGenreState = !currentState
        
        if !currentState == true {
            selectedGenres.append(trueGenres[indexPath.row].getGenreName)
        } else {
            if let index = selectedGenres.index(of: trueGenres[indexPath.row].getGenreName) {
                selectedGenres.remove(at: index)
            }
        }
        
        tableview_Genres.reloadData()
    }
    
    @IBAction func backToEditGenres(segue: UIStoryboardSegue) {
        if let source = segue.source as? DJProfileAddGenreViewController {
            var genres: [String] = []
            
            for genre in trueGenres {
                genres.append(genre.getGenreName)
            }
            
            for word in source.selectedGenres {
                if !genres.contains(word) {
                    trueGenres.append(Genre(name: word, state: false))
                }
            }
            
            self.tableview_Genres.reloadData()
            
            var genreString = ""
            for (offset, genre) in self.trueGenres.enumerated() {
                if offset != self.trueGenres.count - 1 {
                    genreString.append("\(genre.getGenreName), ")
                } else {
                    genreString.append(genre.getGenreName)
                }
            }
            
            self.ref.setValue(genreString)
        }
    }
    
    
}
