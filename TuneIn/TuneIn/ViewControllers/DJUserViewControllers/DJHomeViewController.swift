//
//  DJHomeViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 12/16/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation

class DJHomeViewController: UIViewController, CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate  {
    @IBOutlet weak var MenuItemTrash: UIBarButtonItem!
    
    // MARK: Outlets
    @IBOutlet weak var stackview_Location: UIStackView!
    @IBOutlet weak var label_Location: UILabel!
    @IBOutlet weak var label_DJName: UILabel!
    @IBOutlet weak var imageview_DJPicture: UIImageView!
    @IBOutlet weak var view_EmptyState: UIView!
    @IBOutlet weak var view_NonEmptyState: UIView!
    @IBOutlet weak var tableview_AcceptedRequests: UITableView!
    @IBOutlet weak var switch_GoingLive: UISwitch!
    
    
    // MARK: Variables
    var alreadyislive = false
    var locationManager: CLLocationManager!
    var ref: DatabaseReference!
    var acceptedSongs : [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Code to round the imageview and set tableview's delegate and datasource
        tableview_AcceptedRequests.dataSource = self
        tableview_AcceptedRequests.delegate = self
        imageview_DJPicture.layer.cornerRadius =  imageview_DJPicture.layer.frame.height / 2 + 1
    }
    

    override func viewWillAppear(_ animated: Bool) {
        removeBorderAddShadowToNavigationBar()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        ref = Database.database().reference().child("dj_users").child(Auth.auth().currentUser?.uid ?? "")
        
        // Code to get email and fullname from current user
        if let userUID = Auth.auth().currentUser?.uid {
            Database.database().reference().child("dj_users").child(userUID).observeSingleEvent(of: .value, with: { (snapshot) in
                
                // Get user fullname
                let value = snapshot.value as? [String : Any]
                
                if let isLive = value?["isLive"] as? Bool{
                    if isLive{
                        self.switch_GoingLive.isOn = true
                        self.alreadyislive  = true
                        self.label_Location.isHidden = false
                        self.determineMyCurrentLocation()
                        self.goingLiveSwitchTapped(self.switch_GoingLive)
                    }else{
                        self.switch_GoingLive.isOn = false
                    }
                }
                if let fullname = value?["fullname"] as? String, let imageUrl = value?["image"] as? String {
                    self.label_DJName.text = fullname
                    
                    let url = URL(string: imageUrl)
                    
                    DispatchQueue.global().async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            
                            if(data != nil){
                                self.imageview_DJPicture.image = UIImage(data: data!)
                            }
                           
                        }
                    }
                }
                
            }) { (error) in
                print(error.localizedDescription)
            }
        }
        
        ref = Database.database().reference()
        ref = Database.database().reference().child("dj_users").child(Auth.auth().currentUser?.uid ?? "").child("approved_songs")
        ref.observe(.value, with: { (snapshot) in
            self.acceptedSongs.removeAll()
            
            guard let shotValue = snapshot.value as? [String: Any] else {
                return
            }
            
            shotValue.forEach({ (key, value) in
                
               
                    self.acceptedSongs.append(value as! String)
                
            })

            self.tableview_AcceptedRequests.reloadData()
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Function to turn on live mode and get DJ's current location
    @IBAction func goingLiveSwitchTapped(_ sender: UISwitch) {
         ref = Database.database().reference()
         ref = Database.database().reference().child("dj_users").child(Auth.auth().currentUser?.uid ?? "")
        if !switch_GoingLive.isOn {
         //   switch_GoingLive.setOn(false, animated: true)
            stackview_Location.isHidden = true
            alreadyislive = false
            // Code to change live status of DJ to off
            ref.child("isLive").setValue(false)
            ref.child("latitude").removeValue()
            ref.child("longitude").removeValue()
        } else {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                // Request when-in-use authorization initially
                locationManager.requestWhenInUseAuthorization()
                break
                
            case .restricted, .denied:
                // Disable location features
                let alert = UIAlertController(title: "Location Access", message: "Tune-In does not have access to your location. Please go to Settings and turn on Location", preferredStyle: .alert)
                let settingsAppUrl = URL(string: UIApplicationOpenSettingsURLString)!
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                alert.addAction(UIAlertAction(title: "Settings", style: .cancel, handler: { (action) in
                    UIApplication.shared.open(settingsAppUrl, options: [:], completionHandler: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                switch_GoingLive.setOn(false, animated: true)
                break
                
            case .authorizedWhenInUse:
                // Enable basic location features
                let feedback = UIAlertController(title: "", message: "Are you sure you want to go live at this location?", preferredStyle: UIAlertControllerStyle.alert)
                feedback.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                    self.switch_GoingLive.setOn(false, animated: true)
                }))
                
                feedback.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    //   self.switch_GoingLive.setOn(true, animated: true)
                    self.determineMyCurrentLocation()
                }))
                if(!alreadyislive){
                    self.present(feedback, animated: true, completion: nil)
                }
                break
                
            case .authorizedAlways:
                // Enable any of your app's location features
                
                let feedback = UIAlertController(title: "", message: "Are you sure you want to go live at this location?", preferredStyle: UIAlertControllerStyle.alert)
                feedback.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                    self.switch_GoingLive.setOn(false, animated: true)
                }))
                
                feedback.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    //   self.switch_GoingLive.setOn(true, animated: true)
                    self.determineMyCurrentLocation()
                }))
                if(!alreadyislive){
                self.present(feedback, animated: true, completion: nil)
                }
                break
            }
           
        }
    }
    
    // Function to create instance for CLLocationManager, set delegate to this class to find current location of user
    func determineMyCurrentLocation() {
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    // Function to get the current location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation: CLLocation = locations[0] as CLLocation
         ref = Database.database().reference()
         ref = Database.database().reference().child("dj_users").child(Auth.auth().currentUser?.uid ?? "")
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        // Code to change live status of DJ to on
        
        ref.child("latitude").setValue(userLocation.coordinate.latitude)
        ref.child("longitude").setValue(userLocation.coordinate.longitude)
        ref.child("isLive").setValue(true)
        findTrueLocation(coordinate: userLocation)
        
        
        
        // Call stopUpdatingLocation() to stop listening for location updates otherwise this function will be called every time when user location changes
         manager.stopUpdatingLocation()
    }
    
    // Function to find location name from coordinates
    func findTrueLocation(coordinate: CLLocation) {
        
        // Add below code to get address for touch coordinates.
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: coordinate.coordinate.latitude, longitude: coordinate.coordinate.longitude)
        geoCoder.reverseGeocodeLocation(location, completionHandler:
            {
                placemarks, error -> Void in
                
                // Place details
                if let placemarks = placemarks {
                    guard let placeMark = placemarks.first else { return }
                    
                    // Get city and state
                    if let city = placeMark.locality, let state = placeMark.administrativeArea {
                        self.label_Location.text = "\(city), \(state)"
                        self.ref.child("city").setValue(city)
                        self.ref.child("state").setValue(state)
                        self.stackview_Location.isHidden = false
                        
                        let userID = Auth.auth().currentUser?.uid
                        self.ref = Database.database().reference()
                        self.ref.child("dj_users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
                            // Get user value
                            let value = snapshot.value as? NSDictionary
                            let followers = value?["followers"] as? [String : Any]
                            if(followers != nil && !(followers?.isEmpty)!){
                            for key in (followers?.keys)!{
                               
                                let msg = "\(self.label_DJName.text ?? "") has gone live in \(city), \(state)"
                                self.ref.child("regular_users").child(key).child("notifications").child("live_djs").child(userID!).setValue(msg)
                            }
                            }
                            
                        }) { (error) in
                            print(error.localizedDescription)
                        }
                    }
                }
        })
    }
    
    // Function to print out error if something happens to the getting location name process
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(acceptedSongs.isEmpty){
            view_EmptyState.isHidden = false
            view_NonEmptyState.isHidden = true
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        } else {
            view_EmptyState.isHidden = true
            view_NonEmptyState.isHidden = false
        }
        
        return acceptedSongs.count
    }
    
    // Modify each row of tableview
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = acceptedSongs[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
            return true
    }
  
    @IBAction func ClearSongs(_ sender: Any) {
        let userID = Auth.auth().currentUser?.uid
        ref = Database.database().reference()
        self.ref.child("dj_users").child(userID!).child("approved_songs").removeValue()
        tableview_AcceptedRequests.reloadData()
    }
}
