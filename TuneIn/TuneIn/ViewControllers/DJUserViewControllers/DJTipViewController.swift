//
//  DJTipViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 1/9/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import Firebase

class DJTipViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    // MARK: Outlet
    @IBOutlet weak var TipsTableView: UITableView!
    @IBOutlet weak var view_EmptyState: UIView!
    
    // MARK: Variables
    var tips = [Tip]()
    var deleting: Bool = false
    var ref: DatabaseReference!
    
    // MARK: Conforms UITableView's protocol stubs
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tips.count > 0 {
            view_EmptyState.isHidden = true
        } else {
            view_EmptyState.isHidden = false
        }
        return tips.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = String(format: "Tip Amount: $%.02f", tips[indexPath.row].amount)
        return cell
    }
    
    // Reload tableview when the view appears
    override func viewWillAppear(_ animated: Bool) {
        ref = Database.database().reference().child("dj_users").child((Auth.auth().currentUser?.uid)!)
            .child("tips")
        
        ref.observe(.value, with: { (snapshot) in
            
            
            self.tips.removeAll()
            guard let shotValue = snapshot.value as? [String: Any] else {
                return
            }
            
            shotValue.forEach({ (kkey, value) in
                
               
                guard let realValue = value as? [String: Any] else {
                    return
                }
                realValue.forEach({ (key, rvalue) in
                    
                    let dateArray = key.components(separatedBy: "_")
                    
                    let day = dateArray[0]
                    let month = dateArray[1]
                    let year = dateArray[2]
                    
                    let date = Date()
                    let calendar = Calendar.current
                    var cday = String(calendar.component(.day, from: date))
                    var cmonth = String(calendar.component(.month, from: date))
                    if(cmonth.count == 1){
                        cmonth = "0\(cmonth)"
                    }
                    
                    if cday.count == 1 {
                        cday = "0\(cday)"
                    }
                    
                    let cyear = String(calendar.component(.year, from: date))
                    if(day == cday && month == cmonth && year == cyear){
                        let tip = Tip.init(_amount: rvalue as! Double, _day: Int(day)!, _month: Int(month)!, _year: Int(year)!, _tipper: kkey)
                        
                        self.tips.append(tip)
                    }
                   
                })
                self.TipsTableView.reloadData()
                
            })
            
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    // Change the look of the navigation bar
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        removeBorderAddShadowToNavigationBar()
    }
}
