//
//  DJProfileDataVisualizationViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 1/18/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import Firebase
import Charts

class DJProfileDataVisualizationViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var view_BarChartView: BarChartView!
    @IBOutlet weak var segment_ChartSegment: UISegmentedControl!
    
    // MARK: Variables
    var data: [Tip] = []
    var weekDay: [Int: [Int]] = [:]
    
    var week: [Int: Double] = [1:0.0, 2:0.0, 3:0.0, 4:0.0, 5:0.0]
    var month: [Int: Double] = [1:0.0, 2:0.0, 3:0.0, 4:0.0, 5:0.0, 6:0.0, 7:0.0, 8:0.0, 9:0.0, 10:0.0, 11:0.0, 12:0.0]
    var year: [Int: Double] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        removeBorderAddShadowToNavigationBar()
        self.navigationController?.navigationBar.tintColor = UIColor(hexCode: "#260909")
        currrentSegment()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func segmentTapped(_ sender: UISegmentedControl) {
        currrentSegment()
    }
    
    // Function to separate data into appropriate categories: weekly, monthly, yearly
    func currrentSegment() {
        if let uid = Auth.auth().currentUser?.uid {
            self.data.removeAll()
            let date = Date()
            let calendar = Calendar.current
            
            let year = calendar.component(.year, from: date)
            let month = calendar.component(.month, from: date)
            
            if self.segment_ChartSegment.selectedSegmentIndex == 0 {
                weekDay = [:]
                let numOfWeek = calendar.range(of: .weekOfMonth, in: .month, for: date)
                let numOfDay = calendar.range(of: .day, in: .month, for: date)
                let startDay = 1
                
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy/MM/dd"
                
                if let numOfWeek = numOfWeek, let numOfDay = numOfDay {
                    for i in 0..<numOfWeek.count {
                        var chosenDay = startDay + (7 * i)
                        
                        if chosenDay > numOfDay.count {
                            chosenDay = numOfDay.count
                        }
                        
                        let givenDate = formatter.date(from: "\(year)/\(month)/\(chosenDay)")
                        if let start = givenDate?.startOfWeek, let end = givenDate?.endOfWeek {
                            var startWeek = calendar.component(.day, from: start)
                            let startMonth = calendar.component(.month, from: start)
                            var endWeek = calendar.component(.day, from: end)
                            let endMonth = calendar.component(.month, from: end)
                            
                            if startWeek != startDay, startMonth != month {
                                startWeek = startDay
                            }
                            
                            if endWeek != numOfDay.count, endMonth != month {
                                endWeek = numOfDay.count
                            }
                            
                            weekDay[i + 1] = Array(startWeek...endWeek)
                        }
                    }
                }
                
                Database.database().reference().child("dj_users").child(uid).child("tips").observe(.value, with: { (snapshot) in
                    if let snapshot = snapshot.value as? [String: [String: Double]] {
                        self.week = [1:0.0, 2:0.0, 3:0.0, 4:0.0, 5:0.0]
                        
                        snapshot.forEach({ (outerKey, value) in
                            value.forEach({ (subKey, subValue) in
                                let separateComponent = subKey.components(separatedBy: "_")
                                if Int(separateComponent[1]) == month, Int(separateComponent[2]) == year {
                                
                                    let key = self.searchKey(for: Int(separateComponent[0])!, in: self.weekDay)
                                    
                                    if let value = self.week[key] {
                                        self.week[key] = value + subValue
                                    }
                                    
                                    self.setBarChart(count: 5, dict: self.week)
                                    self.data.append(Tip(_amount: subValue, _day: Int(separateComponent[0])!, _month: month, _year: year, _tipper: outerKey))
                                }
                            })
                        })
                    }
                }, withCancel: { (error) in
                    print(error.localizedDescription)
                })
                
            } else {
                Database.database().reference().child("dj_users").child(uid).child("tips").observe(.value, with: { (snapshot) in
                    if let firstValue = snapshot.value as? [String: [String: Double]] {
                        if self.segment_ChartSegment.selectedSegmentIndex == 1 {
                            self.month = [1:0.0, 2:0.0, 3:0.0, 4:0.0, 5:0.0, 6:0.0, 7:0.0, 8:0.0, 9:0.0, 10:0.0, 11:0.0, 12:0.0]
                            firstValue.forEach({ (outerKey, value) in
                                    value.forEach({ (subKey, subValue) in
                                        let separateComponent = subKey.components(separatedBy: "_")
                                        print(separateComponent)
                                        if Int(separateComponent[1]) == month, Int(separateComponent[2]) == year {
                                            if let amountValue = self.month[Int(separateComponent[1])!] {
                                                self.month[Int(separateComponent[1])!] = amountValue + subValue
                                            } else {
                                                self.month[Int(separateComponent[1])!] = subValue
                                            }
                                        }
                                    })
                                
                            })
                            self.setBarChart(count: 12, dict: self.month)
                        } else {
                            self.year = [:]
                            
                            firstValue.forEach({ (key, value) in
                                value.forEach({ (subKey, subValue) in
                                    let separateComponent = subKey.components(separatedBy: "_")
                                    
                                    if let amountValue = self.year[Int(separateComponent[2])!] {
                                        self.year[Int(separateComponent[2])!] = amountValue + subValue
                                    } else {
                                        self.year[Int(separateComponent[2])!] = subValue
                                    }
                                })
                            })
                            
                            self.setBarChart(count: self.year.count, dict: self.year)
                        }
                    }
                }) { (error) in
                    print(error.localizedDescription)
                }
            }
            
            
        }
    }
    
    // Function to find the key for given value
    func searchKey(for value: Int, in dict: [Int: [Int]]) -> Int {
        var result = 0
        dict.forEach { (key, dictvalue) in
            if dictvalue.contains(value) {
                result = key
            }
        }
        return result
    }
    
    // Function to set up data for bar chart and modify the bar chart design
    func setBarChart(count: Int, dict: [Int: Double]) {
        
        var values: [BarChartDataEntry] = []
        dict.forEach { (arg) in
            let (key, value) = arg
            values.append(BarChartDataEntry(x: Double(key), y: value))
        }
        
        let chartDataSet = BarChartDataSet(values: values, label: "Tip Amount")
        let chartData = BarChartData()
        chartData.addDataSet(chartDataSet)
        chartDataSet.colors = [UIColor.cyan]
        
        // Axes setup
        view_BarChartView.fitBars = true
        view_BarChartView.xAxis.axisLineColor = UIColor.cyan
        view_BarChartView.xAxis.labelPosition = .bottom
        view_BarChartView.xAxis.drawGridLinesEnabled = false
        view_BarChartView.chartDescription?.enabled = false
        view_BarChartView.legend.enabled = true
        view_BarChartView.rightAxis.enabled = false
        view_BarChartView.leftAxis.drawGridLinesEnabled = false
        view_BarChartView.leftAxis.drawLabelsEnabled = true
        view_BarChartView.leftAxis.enabled = false
        
        view_BarChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        self.view_BarChartView.data = chartData
    }
}

// Extension to get the start date and end date of week for a given Date object
extension Date {
    
    var startOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 1, to: sunday)
    }
    
    var endOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
}
