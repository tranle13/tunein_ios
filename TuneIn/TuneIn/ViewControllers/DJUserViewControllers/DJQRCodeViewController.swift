//
//  DJQRCodeViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 1/9/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import FirebaseAuth

class DJQRCodeViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var imageviewQR: UIImageView!
    @IBOutlet weak var segmentCon: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        removeBorderAddShadowToNavigationBar()
        changeDisplayedQR(segmentCon)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Function to display the app QR code and
    @IBAction func changeDisplayedQR(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            imageviewQR.image = UIImage(named: "TestQRCode")
        } else {
            let data = ("realdj+" + (Auth.auth().currentUser?.uid)!).data(using: .ascii, allowLossyConversion: false)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setValue(data, forKey: "inputMessage")
        let ciImage = filter?.outputImage
        
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let transformImage = ciImage?.transformed(by: transform)
        
        let image = UIImage(ciImage: transformImage!)
    
            imageviewQR.image = image
            
        }
        
    }
}
