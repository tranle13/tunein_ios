//
//  DJProfileViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 1/9/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import Firebase

class DJProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // MARK: Outlets
    @IBOutlet weak var button_LogOut: UIButton!
    @IBOutlet weak var imageview_ProfilePicture: UIImageView!
    @IBOutlet weak var label_DJName: UILabel!
    @IBOutlet weak var label_DJEmail: UILabel!
    @IBOutlet weak var tableview_OtherFunc: UITableView!
    @IBOutlet weak var tableviewHeightConstraint: NSLayoutConstraint!
    
    // MARK: Variables
    let otherFunctionatlities = ["Edit Profile", "Genres", "Notifications", "Tip Data Visualization"]
    var email: String!
    var name: String!
    
    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        removeBorderAddShadowToNavigationBar()
        
        roundButtons(buttons: [button_LogOut])
        imageview_ProfilePicture.layer.cornerRadius =  imageview_ProfilePicture.frame.height / 2 + 1
        button_LogOut.layer.borderWidth = 1
        button_LogOut.layer.borderColor = UIColor(hexCode: "#260909").cgColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Code to get email and fullname from current user
        if let userUID = Auth.auth().currentUser?.uid, let userEmail = Auth.auth().currentUser?.email {
            label_DJEmail.text = userEmail
            self.email = userEmail
            Database.database().reference().child("dj_users").child(userUID).observeSingleEvent(of: .value, with: { (snapshot) in
                
                // Get user fullname
                let value = snapshot.value as? [String : Any]
                
                if let fullname = value?["fullname"] as? String, let imageUrl = value?["image"] as? String {
                    let url = URL(string: imageUrl)
                    
                    DispatchQueue.global().async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            
                            if(data != nil){
                                 self.imageview_ProfilePicture.image = UIImage(data: data!)
                            }
                           
                        }
                    }
                    self.label_DJName.text = fullname
                    self.name = fullname
                }
                
            }) { (error) in
                print(error.localizedDescription)
            }
        }
    }
    
    // Function to log out for DJ
    @IBAction func logoutTapped(_ sender: UIButton) {
        do {
            try Auth.auth().signOut()
            UserDefaults.standard.removeObject(forKey: "userEmail")
            UserDefaults.standard.removeObject(forKey: "userPassword")
            performSegue(withIdentifier: "BackToBeginning", sender: self)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    // MARK: Conform UITableView's protocol stubs
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return otherFunctionatlities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "djOtherFuncCell", for: indexPath)
        cell.textLabel?.text = otherFunctionatlities[indexPath.row]
        
        return cell
    }
    
    // Function to resize tableview as the contentsize
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    // Function to resize tableview as the contentsize
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.tableviewHeightConstraint.constant = self.tableview_OtherFunc.contentSize.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            performSegue(withIdentifier: "ToDJEditProfile", sender: self)
        } else if indexPath.row == 1 {
            performSegue(withIdentifier: "ToViewGenre", sender: self)
        }else if indexPath.row == 2 {
            performSegue(withIdentifier: "ToNotifications", sender: self)
        }
        else if indexPath.row == 3 {
            performSegue(withIdentifier: "ToDataVisualization", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DJEditProfileViewController {
            destination.name = self.name
            destination.email = self.email
            destination.image = imageview_ProfilePicture.image
        }
        if let destination =  segue.destination as? RegularNotificationsTableViewController{
            destination.isDJ = true
        }
    }
}
