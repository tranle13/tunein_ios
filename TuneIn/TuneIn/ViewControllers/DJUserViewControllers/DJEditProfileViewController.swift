//
//  DJEditProfileViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 1/11/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation

class DJEditProfileViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    // MARK: Outlets
    @IBOutlet weak var imageview_DJImage: UIImageView!
    @IBOutlet weak var textfield_DJName: CustomTextField!
    @IBOutlet weak var textfield_DJEmail: CustomTextField!
    
    // MARK: Variables
    var name: String!
    var email: String!
    var image: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
        textfield_DJEmail.text = email
        textfield_DJName.text = name
        imageview_DJImage.image = image
        imageview_DJImage.layer.cornerRadius = imageview_DJImage.frame.height / 2 + 1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    // Code to get library or camera to change profile picture
    @IBAction func changeProfileImageTapped(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: "Change profile picture", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.checkAuthorization(type: .camera)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            self.checkAuthorization(type: .photoLibrary)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // Function to present Camera if permission is granted
    func presentPhotoAndCamera(type: UIImagePickerControllerSourceType) {
        let pickerController = UIImagePickerController()
        if type == .camera {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                pickerController.sourceType = .camera
            }
        } else {
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                pickerController.sourceType = .photoLibrary
            }
        }
        
        pickerController.delegate = self
        self.present(pickerController, animated: true, completion: nil)
    }
    
    // Function to request for camera's permission when the status is undetermined
    func requestPhotoPermission(type: UIImagePickerControllerSourceType) {
        AVCaptureDevice.requestAccess(for: .video) { (granted) in
            guard granted == true else { return }
            self.presentPhotoAndCamera(type: type)
        }
    }
    
    // Function to display an alert notifying user to allow Camera in Settings
    func alertCameraAccessNeeded(type: UIImagePickerControllerSourceType) {
        let settingsAppUrl = URL(string: UIApplicationOpenSettingsURLString)!
        
        let alert = UIAlertController(title: "Camera Access", message: "Tune-In does not have access to your camera. Please go to Settings and turn on Camera", preferredStyle: .alert)
        
        if type == .photoLibrary {
            alert.message = "Tune-In does not have access to your photo library. Please go to Settings and allow access to Photos"
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel, handler: { (action) in
            UIApplication.shared.open(settingsAppUrl, options: [:], completionHandler: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // Check for camera authorization
    func checkAuthorization(type: UIImagePickerControllerSourceType) {
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        switch status {
            
        // If user already authorizes it, open the camera
        case .authorized:
            self.presentPhotoAndCamera(type: type)
            
        // If user hasn't, ask for permission again
        case .notDetermined:
            self.requestPhotoPermission(type: type)
            
        // If user has denied access before
        case .denied, .restricted:
            alertCameraAccessNeeded(type: type)
        }
    }
    
    // Function to get taken image from camera or chosen one from photo library
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            print("No image found")
            return
        }
        picker.dismiss(animated: true, completion: nil)
        imageview_DJImage.image = image.resized(withPercentage: 220/image.size.width)
    }
    
    // Function to save changes to Firebase
    @IBAction func saveTapped(_ sender: UIBarButtonItem) {
        var ref: DatabaseReference!
        ref = Database.database().reference()
        let userID = Auth.auth().currentUser?.uid
        if((textfield_DJName.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! || (textfield_DJEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)!){
            let alert = UIAlertController(title: "Update profile", message: "You can not have a empty name or email!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        if !textfield_DJEmail.text!.isEmpty, !textfield_DJName.text!.isEmpty {
            if textfield_DJEmail.text != email, isValidEmail(testStr: textfield_DJEmail.text!) {
                Auth.auth().currentUser?.updateEmail(to: textfield_DJEmail.text ?? "") { (error) in
                    
                    if (error == nil){
                        self.email = self.textfield_DJEmail.text
                        ref.child("dj_users").child(userID!).child("email").setValue(self.textfield_DJEmail.text)
                    }
                }
                
            }
            
            if (textfield_DJName.text != name){
                name = textfield_DJName.text
                let userID = Auth.auth().currentUser?.uid
                ref.child("dj_users").child(userID!).child("fullname").setValue(textfield_DJName.text)
            }
            
            if self.imageview_DJImage.image != image {
                image = self.imageview_DJImage.image
                
                let storageRef = Storage.storage().reference().child(Auth.auth().currentUser?.uid ?? "default value")
                if let image = self.imageview_DJImage.image, let resizedImage = image.resized(withPercentage: 200/image.size.width), let uploadImage = UIImagePNGRepresentation(resizedImage)  {
                    storageRef.putData(uploadImage, metadata: nil, completion: { (metadata, error) in
                        if error != nil {
                            self.createAlert(message: error?.localizedDescription)
                        } else {
                            storageRef.downloadURL(completion: { (url, error) in
                                if error != nil {
                                    self.createAlert(message: error?.localizedDescription)
                                } else {
                                    if let url = url?.absoluteString {
                                        Database.database().reference().child("dj_users").child(userID!).child("image").setValue(url)
                                    }
                                }
                            })
                        }
                        
                    })
                }
            }
            
            let alert = UIAlertController(title: "Update profile", message: "Saved changes to your profile successfully", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            self.createAlert(message: nil)
        }
    }
    
    // Code to dismiss keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
        
    }
    
    // Code to check if entered email is valid
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
