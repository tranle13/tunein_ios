//
//  DJProfileViewGenreViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 1/13/19.
//  Copyright © 2019 TuneIn. All rights reserved.
//

import UIKit
import Firebase

class DJProfileViewGenreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: Outlets
    @IBOutlet weak var view_EmptyState: UIView!
    @IBOutlet weak var tableview_Genres: UITableView!
    
    // MARK: Variables
    var ref: DatabaseReference!
    var genres: [String] = []

    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let uid = Auth.auth().currentUser?.uid {
            ref = Database.database().reference().child("dj_users").child(uid).child("genres")
            
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                if let genre = snapshot.value as? String {
                    let allGenres = genre.components(separatedBy: ",")
                    self.genres.removeAll()
                    
                    for customKind in allGenres {
                        var kind = customKind
                        while kind.first == " " {
                            kind.removeFirst()
                        }
                        
                        while kind.last == " " {
                            kind.removeLast()
                        }
                        
                        self.genres.append(kind)
                    }
                    
                    self.tableview_Genres.reloadData()
                }
            }) { (error) in
                print(error.localizedDescription)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Conforms UITableView's protocol stubs
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if genres.count > 0 {
            view_EmptyState.isHidden = true
        } else {
            view_EmptyState.isHidden = false
        }
        return genres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profileGenreCell", for: indexPath)
        cell.textLabel?.text = genres[indexPath.row]
        
        return cell
    }
}
