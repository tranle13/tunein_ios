//
//  ViewController.swift
//  TuneIn
//
//  Created by Sunny Le on 12/4/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import SafariServices
import GoogleSignIn

class ViewController: UIViewController, UITextFieldDelegate, WKUIDelegate, GIDSignInUIDelegate, GIDSignInDelegate {
    
    // MARK: Outlets
    @IBOutlet weak var txt_Email: CustomTextField!
    @IBOutlet weak var txt_Password: CustomTextField!
    @IBOutlet weak var btn_Instagram: UIButton!
    @IBOutlet weak var btn_Facebook: UIButton!
    @IBOutlet weak var btn_ForgotPass: UIButton!
    @IBOutlet weak var btn_SignIn: UIButton!
    @IBOutlet weak var btn_Register: UIButton!
    
    // MARK: Variables
    var ref: DatabaseReference!
    var loginButton: FBSDKLoginButton!
    var credential: AuthCredential!
    var test: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        test  = self
        let email = UserDefaults.standard.string(forKey: "userEmail")
        let password = UserDefaults.standard.string(forKey:"userPassword")
        
        if email != nil && password != nil {
            login(mEmail: email!, mPassword: password!)
        }
        
        // Rounded Sign in, Register buttons and give them shadow
        ref = Database.database().reference()
        roundButtons(buttons: [btn_SignIn, btn_Register])
        btn_Register.layer.borderWidth = 1
        btn_Register.layer.borderColor = UIColor(hexCode: "#260909").cgColor
        
        // Set content mode of button's image view to scaleAspectFit
        btn_Facebook.imageView?.contentMode = .scaleAspectFit
        btn_Instagram.imageView?.contentMode = .scaleAspectFit
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Code to go to next textfields and sign in when user clicks Go on keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
             nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            if textField.tag == 1 {
                signIn(UIButton())
            }
        }
        return true
    }

    // Code to dismiss the keyboard when user touches the screen
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    // Sign in with email and password
    @IBAction func signIn(_ sender: UIButton) {
        
        // Check if all fields are filled
        UserDefaults.standard.set(txt_Email.text!, forKey: "userEmail")
        UserDefaults.standard.set(txt_Password.text!, forKey: "userPassword")
        if checkTextField(texts: [txt_Email.text!, txt_Password.text!]) {
            if !(txt_Email.text?.isEmpty)!, !(txt_Password.text?.isEmpty)! {
                let email = txt_Email.text!
                let password = txt_Password.text!
                
                login(mEmail: email, mPassword: password)
               
        // If some of the fields are empty, inform user with alert
        } else {
            createAlert(message: nil)
        }
    }
    
    }
    
    func login(mEmail: String,  mPassword : String)
    {
        // Sign user in with Firebase Authentication
        Auth.auth().fetchProviders(forEmail: mEmail) { (providers, error) in
            if providers == nil {
                print("Log in failed!")
                self.createAlert(message: "This account hasn't been created yet")
            } else {
                Auth.auth().signIn(withEmail: mEmail, password: mPassword, completion: { (authResults, error) in
                    if let user = authResults?.user {
                        self.CheckUserThenSignIn(user: user)
                        
                    } else {
                        self.createAlert(message: error?.localizedDescription)
                        print("Check data again")
                    }
                })
            }
        }
    }

    // Code to bring the email to the EmailResetViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? EmailResetViewController {
        
            destination.emailString = txt_Email.text!
        } else if let destination = segue.destination as? LoginChoiceViewController {
            destination.credential = self.credential
        }
        txt_Password.text = ""
        txt_Email.text = ""
    }
    
    @IBAction func rewindToIntroduction(segue: UIStoryboardSegue) {
        // Function to dismiss all view controllers to go back to the beginning sign in page
    }
    
    // Function to check if user is a DJ or not
    func CheckUserThenSignIn(user: User){
       
//        let userID = Auth.auth().currentUser?.uid
        ref.child("dj_users").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? [String : Any]
          
            let keyExists = value?[user.uid] != nil
            let token : [String: AnyObject] = [Messaging.messaging().fcmToken!: Messaging.messaging().fcmToken as AnyObject]

            
            
            self.postToken(Token: token)
            if(keyExists){
                self.performSegue(withIdentifier: "ToHomeScreen2", sender: self)

            } else {
                self.performSegue(withIdentifier: "ToHomeScreen1", sender: self)
            }
            
           
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    func postToken(Token: [String: AnyObject]){
        let dbRef = Database.database().reference()
        dbRef.child("fcmToken").child((Auth.auth().currentUser?.uid)!).setValue(Token)
    }
    
    // Code to let user log in with Facebook account
    @IBAction func facebookLoginTapped(_ sender: UIButton) {
        let fbLoginManager = FBSDKLoginManager()

        
        if(credential == nil){
            fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
                if error != nil {
                    print((error?.localizedDescription)!)
                    self.createAlert(message: "Something happened. Please try again later")
                } else {
                    if let trueResult = result, !trueResult.isCancelled, trueResult.token.hasGranted("public_profile"), trueResult.token.hasGranted("email") {
                        
                        
                        self.credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                        self.test.performSegue(withIdentifier: "ToLoginChoice", sender: nil)
                    }
                }
            }
        }else{
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                // ...
                fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
                    if error != nil {
                        print((error?.localizedDescription)!)
                        self.createAlert(message: "Something happened. Please try again later")
                    } else {
                        if let trueResult = result, !trueResult.isCancelled, trueResult.token.hasGranted("public_profile"), trueResult.token.hasGranted("email") {
                            
                            
                            self.credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                            self.test.performSegue(withIdentifier: "ToLoginChoice", sender: nil)
                        }
                    }
                }
                
                return
            }
            // User is signed in
            // ...
            self.test.performSegue(withIdentifier: "ToLoginChoice", sender: self)
        }
        }
        print("done")
    }
    

    // Code for user to sign in through Google
    @IBAction func googleLoginTapped(_ sender: UIButton) {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error.localizedDescription)
            return
        }
        
        guard let authentication = user.authentication else { return }
        credential = GoogleAuthProvider.credential(withIDToken: (authentication.idToken)!, accessToken: (authentication.accessToken)!)
        performSegue(withIdentifier: "ToLoginChoice", sender: self)
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        print("Dispatch got called")
    }
}

