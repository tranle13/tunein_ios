//
//  File.swift
//  TuneIn
//
//  Created by Sunny Le on 12/5/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension UIViewController {
    
    // Function to check if textfields are empty
    func checkTextField(texts: [String]) -> Bool {
        for text in texts {
            let newString = text.replacingOccurrences(of: " ", with: "")
            if newString.isEmpty {
                return false
            }
        }
        
        return true
    }
    
    // Function to round buttons
    func roundButtons(buttons: [UIButton]) {
        for button in buttons {
            button.layer.cornerRadius = button.bounds.size.height / 2 + 1
            button.layer.shadowColor = UIColor.black.cgColor
            button.layer.shadowRadius = 5
            button.layer.shadowOffset = CGSize(width: 0, height: 3)
            button.layer.shadowOpacity = 0.32
        }
    }
    
    // Function to display alert for empty fields
    func createAlert(message: String?) {
        let feedback = UIAlertController(title: "Error", message: "Please fill in all fields", preferredStyle: .alert)
        if message != nil {
            feedback.message = message
        }
        feedback.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(feedback, animated: true, completion: nil)
    }
    
    // Function to ask for Apple Pay access
    func switchOnApplePay(applePaySwitch: UISwitch) {
        applePaySwitch.setOn(false, animated: false)
        let feedback = UIAlertController(title: "Tune-In Would Like To Access Your Apple Pay", message: "This allows user to tip the DJs faster and more convenient", preferredStyle: UIAlertControllerStyle.alert)
        feedback.addAction(UIAlertAction(title: "Don't Allow", style: .cancel, handler: { (action) in
            applePaySwitch.setOn(false, animated: false)
        }))
        
        feedback.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            applePaySwitch.setOn(true, animated: true)
        }))
        self.present(feedback, animated: true, completion: nil)
    }
    
    // Code to check if password contains at least one uppercase letter, at least one lowercased letter, at least a number and has to have 8 or more characters
    func checkPassword(password: String) -> Bool {
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: password)
        print("\(capitalresult)")
        
        
        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluate(with: password)
        print("\(numberresult)")
        
        
        let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        let specialresult = texttest2.evaluate(with: password)
        print("\(specialresult)")
        
        return capitalresult || numberresult || specialresult
    }
    
    // Function to remove border of navigation bar
    func removeBorderAddShadowToNavigationBar() {
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.black.cgColor
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.15
    }
    
    func generateQRCode(string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    func uploadAndGetDownloadUrl(uid: String, image: UIImage, emailTextField: UITextField, passwordTextField: UITextField, isDJ: Bool) {
        let percentage: CGFloat = image.size.width / 220
        // Upload image to storage first
        let storageRef = Storage.storage().reference().child(uid)
        if let uploadImage = UIImagePNGRepresentation((image.resized(withPercentage: percentage)!)) {
            storageRef.putData(uploadImage, metadata: nil, completion: { (metadata, error) in
                storageRef.downloadURL(completion: { (url, error) in
                    if error != nil {
                        self.createAlert(message: error?.localizedDescription)
                    } else {
                        UserDefaults.standard.set(emailTextField.text!, forKey: "userEmail")
                        UserDefaults.standard.set(passwordTextField.text!, forKey: "userPassword")
                        
                        var childName = "regular_users"
                        if isDJ {
                            childName = "dj_users"
                        }
                        if let url = url?.absoluteString {
                            Database.database().reference().child(childName).child(uid).child("image").setValue(url)
                            self.performSegue(withIdentifier: "ToHomeFromDJ", sender: self)
                        }
                    }
                })
            })
        }
    }
}

extension RegularSearchSongViewController {
    // Helper method to download and parse JSON at a URL
    func downloadAndParse(jsonURL: URL, searchTerm: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        self.dispatch.enter()
        self.process.startAnimating()
        self.process.hidesWhenStopped = false
        let task = session.dataTask(with: jsonURL) { (data, response, error) in
            
            // Bail out on error
            if error != nil { return }
            
            guard let response = response as? HTTPURLResponse, response.statusCode == 200, let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    guard let result = json["results"] as? [String: Any] else {
                        print("Cannot get result")
                        return
                    }
                    
                    guard let trackmatches = result["trackmatches"] as? [String: Any] else {
                        print("Cannot get trackmatches")
                        return
                    }
                    
                    guard let track = trackmatches["track"] as? [Any] else {
                        print("Cannot get track")
                        return
                    }
                    
                    for subTrack in track {
                        guard let realTrack = subTrack as? [String: Any], let name = realTrack["name"] as? String, let artist = realTrack["artist"] as? String, let imageObj = realTrack["image"] as? [Any] else {
                            print("Cannot get this sub track")
                            return
                        }
                        
                        for image in imageObj {
                            guard let realImage = image as? [String: Any] else {
                                print("Cannot get the URL")
                                return
                            }
                            
                            guard let url = realImage["#text"] as? String, let size = realImage["size"] as? String else {
                                print("Cannot get the image URL")
                                return
                            }
                            
                            if size == "large" {
                                let song = Track(name: name, artist: artist, cover: url)
                                self.tracks.append(song)
                            }
                        }
                    }
                    
                    self.dispatch.leave()
                }
            } catch {
                print(error.localizedDescription)
            }
            
            self.dispatch.wait()
            DispatchQueue.main.async {
                self.tableview_SongInformation.reloadData()
            }
        }
        task.resume()
        self.process.startAnimating()
        self.process.isHidden = false
    }
}
