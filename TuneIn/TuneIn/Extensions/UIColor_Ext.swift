//
//  UIColor_Ext.swift
//  TuneIn
//
//  Created by Sunny Le on 12/4/18.
//  Copyright © 2018 TuneIn. All rights reserved.
//

import Foundation
import UIKit

// Extension to convert from HEX to RGB
extension UIColor {
    func rgb() -> (red: Float, green: Float, blue: Float, alpha: Float) {
        var red : CGFloat = 0
        var green : CGFloat = 0
        var blue : CGFloat = 0
        var alpha: CGFloat = 0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return (Float(red), Float(green), Float(blue), Float(alpha))
        } else {
            return (0,0,0,1)
        }
    }
    
    convenience init(hexCode: String) {
        let hex = hexCode.trimmingCharacters(in: CharacterSet.init(charactersIn: "#"))
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}
